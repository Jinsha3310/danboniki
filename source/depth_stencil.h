#pragma once

#include <D3D11.h>
#include <assert.h>



class DepthStencilState
{
public:
	enum DEPTH_STENCIL_STATE
	{
		NONE,
		MASK,
		DRAW,

		MAX
	};
	static ID3D11DepthStencilState*    states[MAX];
	static DEPTH_STENCIL_STATE		   nowMode;
public:
	
	static void Initialize(ID3D11Device* device);

	static void Set(DEPTH_STENCIL_STATE mode, ID3D11DeviceContext* context)
	{
		// 深度ステンシル状態を設定。
		context->OMSetDepthStencilState(states[mode], mode == DEPTH_STENCIL_STATE::NONE ? 0 : 1);
	
		nowMode = mode;
	}
	static void Release()
	{
		for (auto &p : states)
		{
			if (p != nullptr) {
				p->Release();
				p = nullptr;
			}
		}
	}
};
