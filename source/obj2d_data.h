#pragma once
#include "vector.h"
#include "texture.h"
#include "primitive.h"
#include <memory>

class AnimeData
{
public:
	float animecnt;
	int   animeframe;

	AnimeData()
	{
		animecnt = 0.0f;
		animeframe = 0;
	}
	~AnimeData() = default;


	bool Animetion(const float elapsed_time, const float cnt_max, const int frame_max);
};

class OBJ2D
{
public:
	AnimeData anim_data;
	std::unique_ptr<texture::Texture> tex;
	std::unique_ptr<Primitive>        shape;

	VECTOR2F pos;
	VECTOR2F size;
	VECTOR2F velocity;

	float scale;
	float angle;

	bool exist;

	OBJ2D(ID3D11Device* device)
	{ 
		pos      = {};
		size     = {};
		velocity = {};

		scale = 0.f;
		angle = 0.f;
	
		shape = std::make_unique<Primitive>(device);
		tex   = std::make_unique<texture::Texture>();
	};

};