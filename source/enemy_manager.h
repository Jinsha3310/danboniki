#pragma once
#include "enemy.h"
#include <vector>

class EnemyManager
{
	EnemyManager(ID3D11Device* device, const char* fbx_filename);
	~EnemyManager() = default;
	std::vector<std::unique_ptr<Enemy>> enemys;
	const int MAX_ENEMY = 3;
public:
	bool loaded = false;
	
	bool Initialize();
	void Update(const float elapsed_time);
	void Render(ID3D11DeviceContext* context, const DirectX::XMFLOAT4X4& view_projection, const VECTOR4F& light, const VECTOR4F& color, bool modeling_method)
	{
		for (std::unique_ptr<Enemy>& enm : enemys)
		{
			if (enm->exist)
			{
				enm->Render(context, view_projection, light, color, modeling_method);
			}
		}
	}

	static EnemyManager& GetInstance(ID3D11Device* device = nullptr, const char* fbx_filename = 0)
	{
		static EnemyManager instance(device, fbx_filename);
		return instance;
	}
};

#define pEnemyManager (EnemyManager::GetInstance())