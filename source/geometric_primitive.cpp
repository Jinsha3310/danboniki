#include "geometric_primitive.h"
#include "resource.h"
#include <assert.h>

GeometricPrimitive::GeometricPrimitive(ID3D11Device* device)
{
	HRESULT hr = S_OK;

	//頂点レイアウト作成
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{ "POSITION" ,0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL" ,  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT elements_num = ARRAYSIZE(layout);

	const char* vs_name = "./DATA/Shaders/geometric_primitive_vs.cso";
	if (!ResourceManager::LoadVertexShaders(device, vs_name, layout, elements_num, vertex_shader.GetAddressOf(), input_layout.GetAddressOf()))
	{
		OutputDebugStringA("*********************************************");
		OutputDebugStringA("LoadVertexShaders error(GeometricPrimitive)");
		OutputDebugStringA("*********************************************");

		assert(!"頂点シェーダ作成失敗(GeometricPrimitive)");
	}

	const char* ps_name = "./DATA/Shaders/geometric_primitive_ps.cso";
	if (!ResourceManager::LoadPixelShaders(device, ps_name, pixel_shader.GetAddressOf())) {
		OutputDebugStringA("*******************************************");
		OutputDebugStringA("LoadPixelShaders error(GeometricPrimitive)");
		OutputDebugStringA("*******************************************");

		assert(!"ピクセルシェーダ作成失敗(GeometricPrimitive)");
	}

	//ラスタライザステート ソリッド描画
	D3D11_RASTERIZER_DESC rasterizer_desc_solid;			// 
	rasterizer_desc_solid.FillMode				= D3D11_FILL_SOLID;
	rasterizer_desc_solid.CullMode				= D3D11_CULL_BACK;
	rasterizer_desc_solid.FrontCounterClockwise = false;
	rasterizer_desc_solid.DepthBias				= 0;
	rasterizer_desc_solid.DepthBiasClamp		= 0.0f;
	rasterizer_desc_solid.SlopeScaledDepthBias	= 0.0f;
	rasterizer_desc_solid.DepthClipEnable		= true;
	rasterizer_desc_solid.MultisampleEnable		= false;
	rasterizer_desc_solid.ScissorEnable			= false;
	rasterizer_desc_solid.AntialiasedLineEnable = false;
	hr = device->CreateRasterizerState(&rasterizer_desc_solid, solid_state.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateRasterizerState error(GeometricPrimitive::solid)\n");
		OutputDebugStringA("***************************************\n");
		assert(!"ラスタライザ作成失敗(GeometricPrimitive::solid)");
	}

	//ラスタライザステート ワイヤー描画
	D3D11_RASTERIZER_DESC rasterizer_desc_wireframe;
	rasterizer_desc_wireframe.FillMode				= D3D11_FILL_WIREFRAME;
	rasterizer_desc_wireframe.CullMode				= D3D11_CULL_BACK;
	rasterizer_desc_wireframe.FrontCounterClockwise = false;
	rasterizer_desc_wireframe.DepthBias				= 0;
	rasterizer_desc_wireframe.DepthBiasClamp		= 0.0f;
	rasterizer_desc_wireframe.SlopeScaledDepthBias	= 0.0f;
	rasterizer_desc_wireframe.DepthClipEnable		= true;
	rasterizer_desc_wireframe.MultisampleEnable		= false;
	rasterizer_desc_wireframe.ScissorEnable			= false;
	rasterizer_desc_wireframe.AntialiasedLineEnable = true;
	hr = device->CreateRasterizerState(&rasterizer_desc_wireframe, wireframe_state.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateRasterizerState error(GeometricPrimitive::wireframe)\n");
		OutputDebugStringA("***************************************\n");
		assert(!"ラスタライザ作成失敗(GeometricPrimitive::wireframe)");
	}

	D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
	depth_stencil_desc.DepthEnable					= true;
	depth_stencil_desc.DepthWriteMask				= D3D11_DEPTH_WRITE_MASK_ALL;
	depth_stencil_desc.DepthFunc					= D3D11_COMPARISON_LESS;
	depth_stencil_desc.StencilEnable				= false;
	depth_stencil_desc.StencilReadMask				= D3D11_DEFAULT_STENCIL_READ_MASK;
	depth_stencil_desc.StencilWriteMask				= D3D11_DEFAULT_STENCIL_WRITE_MASK;
	depth_stencil_desc.FrontFace.StencilFunc		= D3D11_COMPARISON_ALWAYS;
	depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.FrontFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.FrontFace.StencilPassOp		= D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;
	depth_stencil_desc.BackFace.StencilDepthFailOp	= D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilPassOp		= D3D11_STENCIL_OP_KEEP;
	hr = device->CreateDepthStencilState(&depth_stencil_desc, depth_stencil_state.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("*************************************************\n");
		OutputDebugStringA("CreateDepthStencilState error(GeometricPrimitive)\n");
		OutputDebugStringA("*************************************************\n");
		assert(!"深度ステンシル作成失敗(GeometricPrimitive)");
	}


	
}

GeometricPrimitive::~GeometricPrimitive()
{

}

bool GeometricPrimitive::CreateBuffer(ID3D11Device* device, Vertex* vertices, unsigned int vertex_num, unsigned int* indeces, unsigned int index_num)
{
	HRESULT hr = S_OK;

	D3D11_BUFFER_DESC buffer_desc;
	buffer_desc.ByteWidth = sizeof(Vertex) * vertex_num;// 24面ある
	buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	buffer_desc.CPUAccessFlags = 0;
	buffer_desc.MiscFlags = 0;
	buffer_desc.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA subresource_data;
	subresource_data.pSysMem = vertices;
	subresource_data.SysMemPitch = 0;
	subresource_data.SysMemSlicePitch = 0;
	hr = device->CreateBuffer(&buffer_desc, &subresource_data, vertex_buffer.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("*****************************************************\n");
		OutputDebugStringA("CreateBuffer error(GeometricPrimitive::vertex_buffer)\n");
		OutputDebugStringA("*****************************************************\n");
		assert(!"頂点バッファ作成失敗(GeometricPrimitive)");
		return false;
	}

	buffer_desc.ByteWidth = sizeof(unsigned int) * index_num;			// 点が36
	buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	buffer_desc.CPUAccessFlags = 0;
	buffer_desc.MiscFlags = 0;
	buffer_desc.StructureByteStride = 0;
	subresource_data.pSysMem = indeces;
	subresource_data.SysMemPitch = 0;
	subresource_data.SysMemSlicePitch = 0;
	hr = device->CreateBuffer(&buffer_desc, &subresource_data, index_buffer.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("*****************************************************\n");
		OutputDebugStringA("CreateBuffer error(GeometricPrimitive::index_buffer)\n");
		OutputDebugStringA("*****************************************************\n");
		assert(!"インデックスバッファ作成失敗(GeometricPrimitive)");
		return false;
	}

	buffer_desc.ByteWidth = sizeof(Cbuffer);
	buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	buffer_desc.CPUAccessFlags = 0;
	buffer_desc.MiscFlags = 0;
	buffer_desc.StructureByteStride = 0;
	hr = device->CreateBuffer(&buffer_desc, nullptr, constant_buffer.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("********************************************************\n");
		OutputDebugStringA("CreateBuffer error(GeometricPrimitive::constant_buffer)\n");
		OutputDebugStringA("********************************************************\n");
		assert(!"定数バッファ作成失敗(GeometricPrimitive)");
		return false;
	}

	return true;
}

void GeometricPrimitive::Render(ID3D11DeviceContext* device_context,
	const DirectX::XMFLOAT4X4& world_view_projection,
	const DirectX::XMFLOAT4X4& world,
	const VECTOR4F& light,
	const VECTOR4F& color,
	bool            color_in)
{
	UINT strides = sizeof(Vertex);
	UINT offsets = 0;
	
	//https://docs.microsoft.com/en-us/windows/desktop/api/d3d11/nf-d3d11-id3d11devicecontext-updatesubresource
	Cbuffer data;
	data.world_view_projection	= world_view_projection;
	data.world					= world;
	data.light_direction		= light;
	data.material_color			= color;

	device_context->UpdateSubresource(
		constant_buffer.Get(),			// 宛先リソースへのポインタ
		0,								// 宛先サブリソースを識別するゼロから始まるインデックス
		nullptr,						// リソースデータをコピーするする先のサブリソースの部分を定義するボックスへのポインタ。
		&data,							// メモリ内のソースデータへのポインタ。
		0,								// ソースデータの一行のサイズ
		0);								// ソースデータの1深さスライスのサイズ
	device_context->VSSetConstantBuffers(0,1,constant_buffer.GetAddressOf());
	
	device_context->IASetVertexBuffers(0, 1, vertex_buffer.GetAddressOf(), &strides, &offsets);
	device_context->IASetIndexBuffer(index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);

	device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	device_context->IASetInputLayout(input_layout.Get());
	if(color_in) device_context->RSSetState(solid_state.Get());
	else		 device_context->RSSetState(wireframe_state.Get());

	device_context->VSSetShader(vertex_shader.Get(), nullptr, 0);
	device_context->PSSetShader(pixel_shader.Get(), nullptr, 0);
	device_context->OMSetDepthStencilState(depth_stencil_state.Get(), 0);



	D3D11_BUFFER_DESC buffer_desc;
	index_buffer.Get()->GetDesc(&buffer_desc);


	device_context->DrawIndexed(buffer_desc.ByteWidth / sizeof(unsigned int), 0, 0);
}

GeometricCube::GeometricCube(ID3D11Device* device) : GeometricPrimitive(device)
{
	Vertex vertices[24] = {};
	unsigned int indices[36] = {};


	{//上面
		vertices[0].position = VECTOR3F(-0.5f, +0.5f, +0.5f);
		vertices[1].position = VECTOR3F(+0.5f, +0.5f, +0.5f);
		vertices[2].position = VECTOR3F(-0.5f, +0.5f, -0.5f);
		vertices[3].position = VECTOR3F(+0.5f, +0.5f, -0.5f);
		vertices[0].normal = vertices[1].normal = vertices[2].normal = vertices[3].normal = VECTOR3F(0.0f, +1.0f, 0.0f);
		indices[0] = 0; indices[1] = 1; indices[2] = 2;
		indices[3] = 1; indices[4] = 3; indices[5] = 2;
	}

	{//下面
		vertices[4].position = VECTOR3F(-0.5f, -0.5f, +0.5f);
		vertices[5].position = VECTOR3F(+0.5f, -0.5f, +0.5f);
		vertices[6].position = VECTOR3F(-0.5f, -0.5f, -0.5f);
		vertices[7].position = VECTOR3F(+0.5f, -0.5f, -0.5f);
		vertices[4].normal = vertices[5].normal = vertices[6].normal = vertices[7].normal = VECTOR3F(0.0f, -1.0f, 0.0f);
		indices[6] = 4; indices[7] = 7; indices[8] = 5;
		indices[9] = 4; indices[10] = 6; indices[11] = 7;
	}

	{//前面
		vertices[8].position = VECTOR3F(-0.5f, +0.5f, -0.5f);
		vertices[9].position = VECTOR3F(+0.5f, +0.5f, -0.5f);
		vertices[10].position = VECTOR3F(-0.5f, -0.5f, -0.5f);
		vertices[11].position = VECTOR3F(+0.5f, -0.5f, -0.5f);
		vertices[8].normal = vertices[9].normal = vertices[10].normal = vertices[11].normal = VECTOR3F(0.0f, 0.0f, -1.0f);
		indices[12] = 8; indices[13] = 9; indices[14] = 10;
		indices[15] = 9; indices[16] = 11; indices[17] = 10;
	}

	{//後面
		vertices[12].position = VECTOR3F(-0.5f, +0.5f, +0.5f);
		vertices[13].position = VECTOR3F(+0.5f, +0.5f, +0.5f);
		vertices[14].position = VECTOR3F(-0.5f, -0.5f, +0.5f);
		vertices[15].position = VECTOR3F(+0.5f, -0.5f, +0.5f);
		vertices[12].normal = vertices[13].normal = vertices[14].normal = vertices[15].normal = VECTOR3F(0.0f, 0.0f, 1.0f);
		indices[18] = 12; indices[19] = 15; indices[20] = 13;
		indices[21] = 12; indices[22] = 14; indices[23] = 15;
	}

	{//右面
		vertices[16].position = VECTOR3F(+0.5f, +0.5f, -0.5f);
		vertices[17].position = VECTOR3F(+0.5f, +0.5f, +0.5f);
		vertices[18].position = VECTOR3F(+0.5f, -0.5f, -0.5f);
		vertices[19].position = VECTOR3F(+0.5f, -0.5f, +0.5f);
		vertices[16].normal = vertices[17].normal = vertices[18].normal = vertices[19].normal = VECTOR3F(1.0f, 0.0f, 0.0f);
		indices[24] = 16; indices[25] = 17; indices[26] = 18;
		indices[27] = 17; indices[28] = 19; indices[29] = 18;
	}

	{//左面
		vertices[20].position = VECTOR3F(-0.5f, +0.5f, -0.5f);
		vertices[21].position = VECTOR3F(-0.5f, +0.5f, +0.5f);
		vertices[22].position = VECTOR3F(-0.5f, -0.5f, -0.5f);
		vertices[23].position = VECTOR3F(-0.5f, -0.5f, +0.5f);
		vertices[20].normal = vertices[21].normal = vertices[22].normal = vertices[23].normal = VECTOR3F(-1.0f, 0.0f, 0.0f);
		indices[30] = 20; indices[31] = 23; indices[32] = 21;
		indices[33] = 20; indices[34] = 22; indices[35] = 23;
	}

	CreateBuffer(device, vertices, 24, indices, 36);
}

#include <vector>
GeometricCylinder::GeometricCylinder(ID3D11Device* device, const unsigned int slices , const float radius) : GeometricPrimitive(device)
{
    std::vector<Vertex> vertices;
    std::vector<u_int> indices;

    float d = 2.0f*DirectX::XM_PI / slices;
    float r = 0.5f;

    Vertex vertex;
    u_int base_index = 0;

    // top cap centre
    vertex.position = VECTOR3F(0.0f, +0.5f, 0.0f);
    vertex.normal = VECTOR3F(0.0f, +1.0f, 0.0f);
    vertices.push_back(vertex);
    // top cap ring
    for (u_int i = 0; i < slices; ++i)
    {
        float x = r * cosf(i*d);
        float z = r * sinf(i*d);
        vertex.position = VECTOR3F(x, +0.5f, z);
        vertex.normal = VECTOR3F(0.0f, +1.0f, 0.0f);
        vertices.push_back(vertex);
    }
    base_index = 0;
    for (u_int i = 0; i < slices - 1; ++i)
    {
        indices.push_back(base_index + 0);
        indices.push_back(base_index + i + 2);
        indices.push_back(base_index + i + 1);
    }
    indices.push_back(base_index + 0);
    indices.push_back(base_index + 1);
    indices.push_back(base_index + slices);

    // bottom cap centre
    vertex.position = VECTOR3F(0.0f, -0.5f, 0.0f);
    vertex.normal = VECTOR3F(0.0f, -1.0f, 0.0f);
    vertices.push_back(vertex);
    // bottom cap ring
    for (u_int i = 0; i < slices; ++i)
    {
        float x = r * cosf(i*d);
        float z = r * sinf(i*d);
        vertex.position = VECTOR3F(x, -0.5f, z);
        vertex.normal = VECTOR3F(0.0f, -1.0f, 0.0f);
        vertices.push_back(vertex);
    }
    base_index = slices + 1;
    for (u_int i = 0; i < slices - 1; ++i)
    {
        indices.push_back(base_index + 0);
        indices.push_back(base_index + i + 1);
        indices.push_back(base_index + i + 2);
    }
    indices.push_back(base_index + 0);
    indices.push_back(base_index + (slices - 1) + 1);
    indices.push_back(base_index + (0) + 1);

    // side rectangle
    for (u_int i = 0; i < slices; ++i)
    {
        float x = r * cosf(i*d);
        float z = r * sinf(i*d);

        vertex.position = VECTOR3F(x, +0.5f, z);
        vertex.normal = VECTOR3F(x, 0.0f, z);
        vertices.push_back(vertex);

        vertex.position = VECTOR3F(x, -0.5f, z);
        vertex.normal = VECTOR3F(x, 0.0f, z);
        vertices.push_back(vertex);
    }
    base_index = slices * 2 + 2;
    for (u_int i = 0; i < slices - 1; ++i)
    {
        indices.push_back(base_index + i * 2 + 0);
        indices.push_back(base_index + i * 2 + 2);
        indices.push_back(base_index + i * 2 + 1);

        indices.push_back(base_index + i * 2 + 1);
        indices.push_back(base_index + i * 2 + 2);
        indices.push_back(base_index + i * 2 + 3);
    }
    indices.push_back(base_index + (slices - 1) * 2 + 0);
    indices.push_back(base_index + (0) * 2 + 0);
    indices.push_back(base_index + (slices - 1) * 2 + 1);

    indices.push_back(base_index + (slices - 1) * 2 + 1);
    indices.push_back(base_index + (0) * 2 + 0);
    indices.push_back(base_index + (0) * 2 + 1);

    CreateBuffer(device, vertices.data(), static_cast<unsigned int>(vertices.size()), indices.data(), static_cast<unsigned int>(indices.size()));
}

GeometricSphere::GeometricSphere(ID3D11Device* device, unsigned int slices, unsigned int stacks) : GeometricPrimitive(device)
{
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;

	float r = 0.5f;

	Vertex top_vertex;
	top_vertex.position = VECTOR3F(0.0f, r, 0.0f);
	top_vertex.normal	= VECTOR3F(0.0f, 1.0f, 0.0f);

	Vertex bottom_vertex;
	bottom_vertex.position	= VECTOR3F(0.0f, -r, 0.0f);
	bottom_vertex.normal	= VECTOR3F(0.0f, -1.0f, 0.0f);

	vertices.push_back(top_vertex);

	float phi_step		=		 DirectX::XM_PI / stacks;
	float theta_step	= 2.0f * DirectX::XM_PI / slices;

	for (unsigned int i = 1; i <= stacks - 1; ++i)
	{
		float phi = i*phi_step;
		//float rs_phi = r * sinf(phi), rc_phi = r * cosf(phi);

		for (unsigned int j = 0; j <= slices; ++j)
		{
			float theta = j * theta_step;

			Vertex v;
			v.position.x = r*sinf(phi)*cosf(theta);
			v.position.y = r*cosf(phi);
			v.position.z = r*sinf(phi)*sinf(theta);

			DirectX::XMVECTOR p = DirectX::XMLoadFloat3(&v.position);
			DirectX::XMStoreFloat3(&v.normal, DirectX::XMVector3Normalize(p));

			vertices.push_back(v);
		}
	}
	vertices.push_back(bottom_vertex);

	for (UINT i = 1; i <= slices; ++i)
	{
		indices.push_back(0);
		indices.push_back(i + 1);
		indices.push_back(i);
	}


	u_int base_index = 1;
	u_int ring_vertex_count = slices + 1;
	for (u_int i = 0; i < stacks - 2; ++i)
	{
		u_int i_rvc = i*ring_vertex_count;
		u_int i1_rvc = (i + 1)*ring_vertex_count;

		for (u_int j = 0; j < slices; ++j)
		{
			indices.push_back(base_index + i_rvc + j);
			indices.push_back(base_index + i_rvc + j + 1);
			indices.push_back(base_index + i1_rvc + j);

			indices.push_back(base_index + i1_rvc + j);
			indices.push_back(base_index + i_rvc + j + 1);
			indices.push_back(base_index + i1_rvc + j + 1);
		}
	}

	u_int south_pole_index = (u_int)vertices.size() - 1;

	base_index = south_pole_index - ring_vertex_count;

	for (u_int i = 0; i < slices; ++i)
	{
		indices.push_back(south_pole_index);
		indices.push_back(base_index + i);
		indices.push_back(base_index + i + 1);
	}

	CreateBuffer(device, vertices.data(), (int)vertices.size(), indices.data(), (int)indices.size());
}

GeometricCapsule::GeometricCapsule(ID3D11Device* device) : GeometricPrimitive(device)
{

}
GeometricRect::GeometricRect(ID3D11Device* device) : GeometricPrimitive(device)
{
    Vertex vertices[4] = {};
    unsigned int indices[3 * 2] = {};


    vertices[0].position = VECTOR3F(-0.5f, .0f, +0.5f);
    vertices[1].position = VECTOR3F(+0.5f, .0f, +0.5f);
    vertices[2].position = VECTOR3F(-0.5f, .0f, -0.5f);
    vertices[3].position = VECTOR3F(+0.5f, .0f, -0.5f);
    vertices[0].normal = vertices[1].normal =
        vertices[2].normal =
        vertices[3].normal = VECTOR3F(+0.0f, +1.0f, +0.0f);
    indices[0] = 0;	indices[1] = 1;	indices[2] = 2;
    indices[3] = 1;	indices[4] = 3;	indices[5] = 2;

    CreateBuffer(device, vertices, ARRAYSIZE(vertices), indices, ARRAYSIZE(indices));
}

GeometricBoard::GeometricBoard(ID3D11Device* device) : GeometricPrimitive(device)
{
    Vertex vertices[4] = {};
    unsigned int indices[3 * 2] = {};


    vertices[0].position = VECTOR3F(-0.5f, +0.5f, .0f);
    vertices[1].position = VECTOR3F(+0.5f, +0.5f, .0f);
    vertices[2].position = VECTOR3F(-0.5f, -0.5f, .0f);
    vertices[3].position = VECTOR3F(+0.5f, -0.5f, .0f);
    vertices[0].normal = vertices[1].normal =
        vertices[2].normal =
        vertices[3].normal = VECTOR3F(+0.0f, +0.0f, -1.0f);
    indices[0] = 0;	indices[1] = 1;	indices[2] = 2;
    indices[3] = 1;	indices[4] = 3;	indices[5] = 2;

    CreateBuffer(device, vertices, ARRAYSIZE(vertices), indices, ARRAYSIZE(indices));
}

GeometricPrimitive::Vertex GeometricSphere2::_makeVertex(const DirectX::XMFLOAT3& p)
{
    Vertex v;
    float l = sqrtf(p.x*p.x + p.y*p.y + p.z*p.z);
    v.position.x = p.x;
    v.position.y = p.y;
    v.position.z = p.z;
    v.normal.x = p.x / l;
    v.normal.y = p.y / l;
    v.normal.z = p.z / l;
    return v;
}

GeometricSphere2::GeometricSphere2(ID3D11Device* device, u_int div) :
    GeometricPrimitive(device)
{
    //
    //	参考 : http://rudora7.blog81.fc2.com/blog-entry-390.html
    //

#ifdef D3DD_ASSERT
    if (device == nullptr)	return;
    if (div < 2)			return;
#else
    if (device == nullptr)	assert(0 && "D3D11Device ERROR!");
    if (div < 2)			assert(0 && "Sphere div ERROR!");
#endif // !D3DD_ASSERT


    std::vector<Vertex> sphere;
    int TriangleNum = 0;

    FLOAT Radius = 0.5f;

    DirectX::XMFLOAT3 temp;
    std::vector<std::vector<DirectX::XMFLOAT3>> iList;
    std::vector<DirectX::XMFLOAT3> tTest;

    float *radians = new float[div];
    for (u_int i = 0; i <= div - 1; i++)
        radians[i] = DirectX::XMConvertToRadians(90.0f * (div - i - 1) / (div - 1));


    int tNum = div;
    int tP = ((tNum - 2) << 2) + 4;
    TriangleNum += ((((tP - 1) << 2) + tP) << 1);
    TriangleNum += ((tNum - 2) << 1) * (tP << 1);
    TriangleNum += (((tNum - 1) << 1) - 1) << 2;


    //一番上の頂点の定義
    temp.x = .0f;
    temp.y = Radius;
    temp.z = .0f;
    tTest.push_back(temp);
    iList.push_back(tTest);
    tTest.clear();

    //半球を定義する頂点を算出
    for (int i = 1, count = tNum; i < count; i++)
    {
        float tRadius = cosf(radians[i]) * Radius;	//円の半径
        float tHeight = sinf(radians[i]) * Radius;	//円のＹ座標
        for (int j = 0, count2 = tNum; j < count2; j++)
        {
            temp.x = tRadius * cosf(radians[j]);
            temp.y = tHeight;
            temp.z = tRadius * sinf(radians[j]);

            tTest.push_back(temp);
        }
        //1象限の頂点をｚ座標反転	（k = 0）
        //4象限の頂点をｘ座標反転	（k = 1）
        //3象限の頂点をｚ座標反転	（k = 2）
        for (int k = 0; k < 3; k++) {
            for (int L = 0, Now = static_cast<int>(tTest.size()), count3 = tNum - 1; L < count3; L++)
            {
                temp = tTest[Now - 2 - L];
                if (k % 2 == 0) {
                    temp.z = -tTest[Now - 2 - L].z;
                }
                else {
                    temp.x = -tTest[Now - 2 - L].x;
                }
                if (!(k == 2 && L == count3 - 1))
                    tTest.push_back(temp);
            }
        }
        iList.push_back(tTest);
        tTest.clear();
    }
    delete[]	radians;

    //半球を定義する頂点を全てＹ座標反転。（境界球全体の頂点が出る）
    for (int i = 1, count = static_cast<int>(iList.size()); i < count; i++)
    {
        int revi = count - 1 - i;
        for (int j = 0, count2 = static_cast<int>(iList[revi].size()); j < count2; j++)
        {
            temp = iList[revi][j];
            temp.y = -temp.y;
            tTest.push_back(temp);
        }
        iList.push_back(tTest);
        tTest.clear();
    }


    //真上の頂点１つと、1段下の頂点をつなぐ
    for (int i = 0, count = static_cast<int>(iList[1].size()); i < count; i++)
    {
        sphere.push_back(_makeVertex(iList[0][0]));
        sphere.push_back(_makeVertex(iList[1][(i + (i & 1)) % count]));
        sphere.push_back(_makeVertex(iList[1][(i + ((i + 1) & 1)) % count]));
        sphere.push_back(_makeVertex(iList[1][(i + ((i + 1) & 1)) % count]));
        if (i != count - 1) {
            sphere.push_back(_makeVertex(iList[0][0]));
        }
    }

    //真上と真下以外の頂点をつなぐ
    for (int i = 1, count = static_cast<int>(iList.size()); i < count - 2; i++)
    {
        sphere.push_back(_makeVertex(iList[i][0]));
        for (int j = 0, count2 = static_cast<int>(iList[i].size()); j <= count2; j++)
        {
            sphere.push_back(_makeVertex(iList[i][j % count2]));
            sphere.push_back(_makeVertex(iList[i + 1][j % count2]));
        }
        sphere.push_back(_makeVertex(iList[i + 1][0]));
    }

    sphere.push_back(_makeVertex(iList[iList.size() - 1][0]));

    //真下の頂点１つと、1段上の頂点をつなぐ
    for (int i = 0, count = static_cast<int>(iList[iList.size() - 2].size()), Num = static_cast<int>(iList.size()); i < count; i++)
    {
        sphere.push_back(_makeVertex(iList[Num - 1][0]));
        sphere.push_back(_makeVertex(iList[Num - 2][(i + ((i + 1) & 1)) % count]));
        sphere.push_back(_makeVertex(iList[Num - 2][(i + (i & 1)) % count]));
        sphere.push_back(_makeVertex(iList[Num - 2][(i + (i & 1)) % count]));
        if (i != count - 1) {
            sphere.push_back(_makeVertex(iList[Num - 1][0]));
        }
    }

    CreateBuffer(device, sphere.data(), static_cast<unsigned int>(sphere.size()), nullptr, 0);
    numVertices = static_cast<int>(sphere.size());
}


void GeometricSphere2::render(ID3D11DeviceContext* context,
    const DirectX::XMFLOAT4X4& wvp, const DirectX::XMFLOAT4X4& world,
    const DirectX::XMFLOAT4& light_direction, const DirectX::XMFLOAT4& material_color,
    bool bSolid)
{

    //	定数バッファの作成
    Cbuffer cb;
    cb.world_view_projection = wvp;
    cb.world = world;
    cb.light_direction.x = light_direction.x;
    cb.light_direction.y = light_direction.y;
    cb.light_direction.z = light_direction.z;
    cb.material_color.x = material_color.x;
    cb.material_color.y = material_color.y;
    cb.material_color.z = material_color.z;
    context->UpdateSubresource(constant_buffer.Get(), 0, nullptr, &cb, 0, 0);
    context->VSSetConstantBuffers(0, 1, &constant_buffer);

    // 頂点バッファのバインド
    UINT stride = sizeof(Vertex);
    UINT offset = 0;
    context->IASetVertexBuffers(0, 1, &vertex_buffer, &stride, &offset);

    ////	インデックスバッファのバインド
    //context->IASetIndexBuffer(index_buffer, DXGI_FORMAT_R32_UINT, 0);

    //	プリミティブモードの設定(STRIPで行こう！)
    context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

    //	入力レイアウトのバインド
    context->IASetInputLayout(input_layout.Get());

    //	ラスタライザーの設定
    if (bSolid)	context->RSSetState(solid_state.Get());
    else		context->RSSetState(wireframe_state.Get());

    //	シェーダー(2種)の設定
    context->VSSetShader(vertex_shader.Get(), nullptr, 0);
    context->PSSetShader(pixel_shader.Get(), nullptr, 0);

    //	深度テストの設定
    context->OMSetDepthStencilState(depth_stencil_state.Get(), 0);

    //	プリミティブの描画(index付き)
    context->Draw(numVertices, 0);
}