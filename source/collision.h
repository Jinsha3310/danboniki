#pragma once

#include "vector.h"

//***使えるかどうか分かってない***
bool IsHitSegmentVec2(const VECTOR2F& A, const VECTOR2F& B, const VECTOR2F& C, const VECTOR2F& D);
VECTOR2F GetHitSegmentPosVec2(const VECTOR2F& A, const VECTOR2F& B, const VECTOR2F& C, const VECTOR2F& D);
//********************************

bool IsHitCircle(const VECTOR2F& pos1, const VECTOR2F& pos2, const float radius1, const float radius2);
bool IsHitRect(const VECTOR2F& pos1, const VECTOR4F& size1, const VECTOR2F& pos2, const VECTOR4F& size2);
bool IsHitCapsuleCircle(const VECTOR2F& start, const VECTOR2F& end, const VECTOR2F& c_pos, const float radius1, const float radius2);
float GetCapsuleCircleDistance(const VECTOR2F& start, const VECTOR2F& end, const VECTOR2F& pos);


bool IsHitSphere(const VECTOR3F& pos1, const VECTOR3F& pos2, const float radius1, const float radius2);
bool IsHitCube(const VECTOR3F& min1, const VECTOR3F& max1,
	const VECTOR3F& min2, const VECTOR3F& max2);
bool IsHitCapsuleSphere(const VECTOR3F& start, const VECTOR3F& end, const VECTOR3F& s_pos, const float radius1, const float radius2);
float GetCapsuleSphereDistance(const VECTOR3F& start, const VECTOR3F& end, const VECTOR3F& s_pos);