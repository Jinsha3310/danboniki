#include "input.h"
#include "framework.h"

#include <math.h>
#include <algorithm>
#include <assert.h>

using namespace input;


void Keyboard::State()
{
	previous_state = current_state;

	if (static_cast<unsigned short>(GetAsyncKeyState(key)) & 0x8000)
	{
		current_state++;
	}
	else
	{
		current_state = 0;
	}
}


GamePad::GamePad(const int id, const float deadzone_x, const float deadzone_y)
{
	this->id = id;
	DWORD result = XInputGetState(this->id, &current_state.state);
	if (result != ERROR_DEVICE_NOT_CONNECTED)
	{
		current_state.connected = true;
	}

	this->deadzone_y = deadzone_y;
	this->deadzone_x = deadzone_x;
}

void GamePad::Update()
{
	if (!current_state.connected)
		return;

	previous_state = current_state;

	DWORD result = XInputGetState(id, &current_state.state);


	StickState();
	TriggerState();
}

void GamePad::StickState()
{
	current_state.l_stick.x = ApplyDeadZone(static_cast<float>(current_state.state.Gamepad.sThumbLX), MAX_STICKTILT, deadzone_x);

	current_state.l_stick.y = ApplyDeadZone(static_cast<float>(current_state.state.Gamepad.sThumbLY), MAX_STICKTILT, deadzone_y);

	current_state.r_stick.x = ApplyDeadZone(static_cast<float>(current_state.state.Gamepad.sThumbRX), MAX_STICKTILT, deadzone_x);

	current_state.r_stick.y = ApplyDeadZone(static_cast<float>(current_state.state.Gamepad.sThumbRY), MAX_STICKTILT, deadzone_y);
}

void GamePad::TriggerState()
{
	current_state.l_trigger = ApplyDeadZone(static_cast<float>(current_state.state.Gamepad.bLeftTrigger), MAX_TRRIGERTILT, 0.0f);
	current_state.r_trigger = ApplyDeadZone(static_cast<float>(current_state.state.Gamepad.bRightTrigger), MAX_TRRIGERTILT, 0.0f);
}

float GamePad::ApplyDeadZone(float value, const float max_value, const float deadzone)
{
	value /= max_value;

	if (value > -deadzone && value < deadzone)
	{
		return 0.0f;
	}

	return (std::max)((std::min)(value, 1.0f), -1.0f);
}

void Mouse::State(HWND hwnd)
{
	GetCursorPos(&position);
	ScreenToClient(hwnd, &position);

	position.x = (std::max)((std::min)(position.x, SCREEN_WIDTH), (LONG)0);
	position.y = (std::max)((std::min)(position.y, SCREEN_HEIGHT), (LONG)0);

	for (int i = 0; i < 3; ++i)
	{
		previous_state[i] = current_state[i];

		if ((static_cast<unsigned short>(GetAsyncKeyState(buttons[i])) & 0x8000))
		{
			++current_state[i];
		}
		else
		{
			current_state[i] = 0;
		}
	}
}
bool Mouse::PressedState(MouseLabel button)
{
	switch (button)
	{
	case MouseLabel::LEFT_BUTTON:
		return current_state[0] > 0;
		break;
	case MouseLabel::RIGHT_BUTTON:
		return current_state[1] > 0;
		break;
	case MouseLabel::MID_BUTTON:
		return current_state[2] > 0;
		break;
	default:
		break;
	}
	return false;
}
bool Mouse::RisingState(MouseLabel button)
{
	switch (button)
	{
	case MouseLabel::LEFT_BUTTON:
		return current_state[0] > 0 && previous_state[0] == 0;
		break;
	case MouseLabel::RIGHT_BUTTON:
		return current_state[1] > 0 && previous_state[1] == 0;
		break;
	case MouseLabel::MID_BUTTON:
		return current_state[2] > 0 && previous_state[2] == 0;
		break;
	default:
		break;
	}

	return false;
}
bool Mouse::FallingState(MouseLabel button)
{
	switch (button)
	{
	case MouseLabel::LEFT_BUTTON:
		return current_state[0] == 0 && previous_state[0] > 0;
		break;
	case MouseLabel::RIGHT_BUTTON:
		return current_state[1] == 0 && previous_state[1] > 0;
		break;
	case MouseLabel::MID_BUTTON:
		return current_state[2] == 0 && previous_state[2] > 0;
		break;
	default:
		break;
	}

	return false;
}