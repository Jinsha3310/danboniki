#include "enemy.h"
#include"camera.h"

void Enemy::PlayAnimation(ANIMATION_CLIP animation_index)
{
	substance->model->PlayAnimation(animation_index);

	animation_clip = animation_index;
}

void Enemy::Animate(const float elapsed_time)
{
	static ANIMATION_CLIP prev_animation_clip;
	prev_animation_clip = animation_clip;

	switch (state)
	{
	case STATES::STATE_IDLE:
		animation_clip = ANIMATION_CLIP::CLIP_MOVE;
		break;
	case STATES::STATE_MOVE:

		break;
	}


	if (animation_clip != prev_animation_clip)
	{
		PlayAnimation(animation_clip);
	}

	if (!IsPlayAnimation())
	{
		PlayAnimation(animation_clip);

		switch (animation_clip)
		{
		case ANIMATION_CLIP::CLIP_IDLE:
			animation_clip = CLIP_IDLE;
			state = STATES::STATE_IDLE;
			break;
		case ANIMATION_CLIP::CLIP_MOVE:
			animation_clip = CLIP_MOVE;
			state = STATES::STATE_MOVE;
			break;
		}
	}
	substance->model->Animate(elapsed_time);
}

bool Enemy::Initialize()
{
	position = VECTOR3F(_FRand(-80.0f, 80.0f), 0.0f, _FRand(-80.0f, 80.0f));
	scale = VECTOR3F(0.2f, 0.2f, 0.2f);
	rotate = VECTOR3F(0.0f, 0.0f, 0.0f);

	exist = true;
	return true;
}

bool Enemy::Update(const float elapsed_time)
{
	velocity = VECTOR3F(0.0f, 0.0f, 0.0f);

	DirectX::XMMATRIX W;
	{
		DirectX::XMMATRIX S, R, T;
		S = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
		R = DirectX::XMMatrixRotationRollPitchYaw(rotate.x, rotate.y, rotate.z);
		T = DirectX::XMMatrixTranslation(position.x, position.y, position.z);

		W = S * R * T;
	}

	substance->model->CalculateLocalTransform();
	substance->model->CalculateWorldTransform(W);

	//velocity = Player::GetInstance().position - position;
	velocity = NormalizeVec3(velocity) * elapsed_time;

	Animate(elapsed_time);
	//position += velocity;

	if (!exist)
		return true;
	p_Camera.SetLook_no(this);
	return false;
}