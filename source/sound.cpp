#include "sound.h"
#include <algorithm>


namespace sound
{
	bool Sound::LoadSound(const wchar_t* filename, const float volume)
	{
		this->volume = volume;

		sound_effect = std::make_unique<DirectX::SoundEffect>(audio_engine.get(), filename);
		
		return (sound_effect != nullptr);
	}

	void Sound::Release()
	{
		if (audio_engine)
		{
			audio_engine.reset();
			audio_engine = nullptr;
		}
		if (sound_effect)
		{
			sound_effect.reset();
			sound_effect = nullptr;
		}
		sound_effect_instance.reset();
		
		volume = 0.0f;
	}

	void  Sound::Play(bool isloop)
	{
		if (!sound_effect) return;

		sound_effect_instance = sound_effect->CreateInstance();
		sound_effect_instance->Play(isloop);
		sound_effect_instance->SetVolume(volume);
	}

	void Sound::Stop()
	{
		if (!sound_effect_instance) return;
			sound_effect_instance->Stop();
	}

	void Sound::SetVolume(const float volume)
	{
		if (!sound_effect_instance) return;

		this->volume = (std::max)((std::min)(volume, 1.0f), 0.0f);
		sound_effect_instance->SetVolume(this->volume);
	}


	int SoundManager::LoadSound(const wchar_t* filename, float volume)
	{
		++handle_num;

		sound_data[handle_num] = std::make_unique<Sound>(filename);
		if (sound_data[handle_num]->LoadSound(filename, volume))
		{
			return handle_num;
		}

		return -1;
	}

	void SoundManager::CheckStop(int num)
	{
		sound_data[num]->Stop();
	}

	void SoundManager::PlayData(int num, bool isloop)
	{
		sound_data[num]->Play(isloop);
	}


	void SoundManager::SetVolume(int num, float volume)
	{
		sound_data[num]->SetVolume(volume);
	}
}