#pragma once
#include "skinned_mesh.h"
#include "vector.h"

#include <memory>
#include <vector>

class Player
{
	Player();
	~Player() {};

	enum ANIMATION_CLIP { CLIP_IDLE, CLIP_WAIT_MOVE, CLIP_ATK, CLIP_FLY };

	ANIMATION_CLIP	animation_clip = CLIP_IDLE;

	bool IsPlayAnimation() const { return substances.at(animation_clip)->model->IsPlayAnimation(); }
	void PlayAnimation(ANIMATION_CLIP animation_index);
	void Animate(const float elapsed_time);
public:
	bool loaded = false;
	std::vector<std::unique_ptr<SkinnedMesh>> substances;

	const float SPEED = 15.0f;

	VECTOR3F scale;
	VECTOR3F rotate;
	VECTOR3F velocity;
	VECTOR3F position;

	enum STATES { STATE_IDLE, STATE_ATK, STATE_FLY };
	STATES state = STATE_IDLE;

	void Update(const float elapsed_time);
	void Render(ID3D11DeviceContext* context, const DirectX::XMFLOAT4X4& view_projection, const VECTOR4F& light, const VECTOR4F& color, bool modeling_method)
	{
		substances.at(animation_clip)->Render(context,view_projection,light,color, modeling_method);
	}

	static Player& GetInstance()
	{
		static Player instance;
		return instance;
	}
};

#define pPlayer (Player::GetInstance())