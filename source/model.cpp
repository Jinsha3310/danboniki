#include "model.h"


Model::Model(std::shared_ptr<ModelResource>& resource)
{
	model_resource = resource;

	const std::vector<ModelData::Bone>& rsc_bones = resource->GetBones();
	bones.resize(rsc_bones.size());
	{
		for (size_t bone_index = 0; bone_index < bones.size(); ++bone_index)
		{
			Bone& bone = bones.at(bone_index);
			const ModelData::Bone& datas_bone = rsc_bones.at(bone_index);
			
			bone.name		= datas_bone.name.c_str();
			if (datas_bone.parent_index >= 0)
				bone.parent = &bones.at(datas_bone.parent_index);
			else
				bone.parent = nullptr;
			bone.scale		= datas_bone.scale;
			bone.rotate		= datas_bone.rotate;
			bone.translate	= datas_bone.translate;
		}
	}

	if (model_resource->GetAnimations().empty())
		end_animation = true;
}

void Model::PlayAnimation(unsigned int animation_index)
{
	if (model_resource->GetAnimationTakes().size() <= animation_index)
		return;

	animation_clip = animation_index;
	current_seconds = 0.0f;
	animation_frame = 0;
	end_animation = false;
}

void Model::Animate(const float elapsed_time)
{
	if (end_animation)
		return;

	

	static unsigned int previous_animation_clip;
	previous_animation_clip = animation_clip;

	if (animation_clip != previous_animation_clip)
	{
		animation_frame = 0;
		current_seconds = 0.0f;
	}

	animation_frame = static_cast<unsigned int>(current_seconds * model_resource->GetAnimationTakes().at(animation_clip).sampling_rate);


	float sampling_time = 1.0f / static_cast<float>(model_resource->GetAnimationTakes().at(animation_clip).sampling_rate);
	unsigned int last_of_frame = model_resource->GetAnimationTakes().at(animation_clip).number_of_frames - 1;
	if (animation_frame > last_of_frame - 1) //アニメーションの終わり
	{
		current_seconds = static_cast<float>(last_of_frame) * sampling_time;
		end_animation = true;

		animation_frame = 0;
		current_seconds = 0.0f;
	}


	//現在のキーフレームと次のキーフレームを取得
	const ModelData::Animation& animation = model_resource->GetAnimations().at(animation_clip);
	const std::vector<ModelData::KeyFrame>& keyframes = animation.keyframes;

	const ModelData::KeyFrame& keyframe0 = keyframes.at(animation_frame);
	const ModelData::KeyFrame& keyframe1 = keyframes.at(animation_frame + 1);

	float keyframe0_second = sampling_time * static_cast<float>(animation_frame);
	float keyframe1_second = sampling_time * static_cast<float>(animation_frame + 1);
	float rate = (current_seconds - keyframe0_second) / (keyframe1_second - keyframe0_second);
	
	assert(bones.size() == keyframe0.bone_datas.size());
	assert(bones.size() == keyframe1.bone_datas.size());

	int bone_count = static_cast<int>(bones.size());
	for (int bone_index = 0; bone_index < bone_count; ++bone_index)
	{
		//２つのキーフレーム間の補完計算
		const ModelData::BoneData& key0 = keyframe0.bone_datas.at(bone_index);
		const ModelData::BoneData& key1 = keyframe1.bone_datas.at(bone_index);

		Bone& bone = bones.at(bone_index);

		DirectX::XMVECTOR s0 = DirectX::XMLoadFloat3(&key0.scale);
		DirectX::XMVECTOR s1 = DirectX::XMLoadFloat3(&key1.scale);
		DirectX::XMVECTOR r0 = DirectX::XMLoadFloat4(&key0.rotate);
		DirectX::XMVECTOR r1 = DirectX::XMLoadFloat4(&key1.rotate);
		DirectX::XMVECTOR t0 = DirectX::XMLoadFloat3(&key0.translate);
		DirectX::XMVECTOR t1 = DirectX::XMLoadFloat3(&key1.translate);

		DirectX::XMVECTOR s = DirectX::XMVectorLerp(s0, s1, rate);
		DirectX::XMVECTOR r = DirectX::XMQuaternionSlerp(r0, r1, rate);
		DirectX::XMVECTOR t = DirectX::XMVectorLerp(t0, t1, rate);

		DirectX::XMStoreFloat3(&bone.scale, s);
		DirectX::XMStoreFloat4(&bone.rotate, r);
		DirectX::XMStoreFloat3(&bone.translate, t);
	}

	current_seconds += elapsed_time;
}

void Model::CalculateLocalTransform()
{
	for (Bone& bone : bones) 
	{
		DirectX::XMMATRIX S, R, T;
		S = DirectX::XMMatrixScaling(bone.scale.x, bone.scale.y, bone.scale.z);
		R = DirectX::XMMatrixRotationQuaternion(DirectX::XMVectorSet(bone.rotate.x, bone.rotate.y, bone.rotate.z, bone.rotate.w));
		T = DirectX::XMMatrixTranslation(bone.translate.x, bone.translate.y, bone.translate.z);


		FLOAT4X4 float4x4_s;
		FLOAT4X4 float4x4_r;
		FLOAT4X4 float4x4_t;

		DirectX::XMStoreFloat4x4(&float4x4_s, S);
		DirectX::XMStoreFloat4x4(&float4x4_r, R);
		DirectX::XMStoreFloat4x4(&float4x4_t, T);
		bone.local_transform = float4x4_s*float4x4_r*float4x4_t;
	}
}

void Model::CalculateWorldTransform(const DirectX::XMMATRIX& world_transform)
{
	for (Bone& bone : bones)
	{
		if (bone.parent != nullptr)
		{
			bone.world_transform = bone.local_transform * bone.parent->world_transform;
		}
		else
		{
			FLOAT4X4 float4x4;
			DirectX::XMStoreFloat4x4(&float4x4, world_transform);
			bone.world_transform = bone.local_transform * float4x4;
		}
	}
}