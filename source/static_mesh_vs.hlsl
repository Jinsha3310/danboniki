#include "static_mesh.hlsli"

VS_OUT main(float4 pos : POSITION, float4 normal : NORMAL, float2 texcoord : TEXCOORD)
{
	VS_OUT vout;
	vout.position = mul(pos, view_projection);

	normal.w = 0;
	float4 N = normalize(mul(normal, world));
	float4 L = normalize(-light_direction);

	vout.color = material_color*0.5 + material_color * max(0, dot(L, N)*0.5);
	//vout.color = material_color * max(0, dot(L, N));
	vout.color.a = material_color.a;


	vout.texcoord = texcoord;

	return vout;
}