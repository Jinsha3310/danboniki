#include "enemy_manager.h"

#include <algorithm>

EnemyManager::EnemyManager(ID3D11Device* device, const char* fbx_filename)
{
	enemys.resize(MAX_ENEMY);
	for (std::unique_ptr<Enemy>& enm : enemys)
	{
		if (!enm)
		{
			enm = std::make_unique<Enemy>();
			enm->substance = std::make_unique<SkinnedMesh>(device, fbx_filename);
		}
	}
}

bool EnemyManager::Initialize()
{
	for (std::unique_ptr<Enemy>& enm : enemys)
	{
		enm->Initialize();
	}

	return true;
}

void EnemyManager::Update(const float elapsed_time)
{
	enemys.erase(std::remove_if(enemys.begin(), enemys.end(), [&elapsed_time](std::unique_ptr<Enemy>& enm) { return enm->Update(elapsed_time); }), enemys.end());
}