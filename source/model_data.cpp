#include "model_data.h"
#include "resource.h"
#include "framework.h"

void GetResourcePath(char(&combinedResourcePath)[256], const char* referrer_filename, const char* referent_filename)
{
	char delimiter[2] = { '/', '\\' };

	std::string path_str(referrer_filename);
	size_t pos = 0;
	for (int i = 0; i < 2; ++i)
	{
		if (pos != std::wstring::npos)
			break;
		pos = path_str.find_last_of(delimiter[i]);
	}
	std::string path = path_str.substr(0, pos + 1);

	pos = 0;

	std::string image_str(referent_filename);
	for (int i = 0; i < 2; ++i)
	{
		if (pos != std::wstring::npos)
			break;
		pos = image_str.find_last_of(delimiter[i]);
	}
	std::string image = image_str.substr(pos + 1, strlen(referent_filename));

	path += image;
	strcpy_s(combinedResourcePath, path.c_str());
}

void GetResourcePathW(wchar_t(&combinedResourcePath)[256], const wchar_t* referrer_filename, const wchar_t* referent_filename)
{
	wchar_t delimiter[2] = { L'/', L'\\' };

	std::wstring path_str(referrer_filename);
	size_t pos = std::wstring::npos;
	for (int i = 0; i < 2; ++i)
	{
		if (pos != std::wstring::npos)
			break;
		pos = path_str.find_last_of(delimiter[i]);
	}
	std::wstring path = path_str.substr(0, pos + 1);

	pos = std::wstring::npos;

	std::wstring image_str(referent_filename);
	for (int i = 0; i < 2; ++i)
	{
		if (pos != std::wstring::npos)
			break;
		pos = image_str.find_last_of(delimiter[i]);
	}
	std::wstring image = image_str.substr(pos + 1, wcslen(referent_filename));

	path += image;
	wcscpy_s(combinedResourcePath, path.c_str());
}

ModelResource::ModelResource(ID3D11Device* device, std::unique_ptr<ModelData> model_data)
{
	data = std::move(model_data);

	materials.resize(data->materials.size());
	{
		for (size_t material_index = 0; material_index < materials.size(); ++material_index)
		{
			ModelResource::Material& material = materials.at(material_index);
			ModelData::Material& datas_material = data->materials.at(material_index);

			material.color = datas_material.color;
			material.CreateShaderResourceView(device, datas_material.name);
		}
	}

	meshes.resize(data->meshes.size());
	{
		for (size_t mesh_index = 0; mesh_index < meshes.size(); ++mesh_index)
		{
			ModelResource::Mesh& mesh	= meshes.at(mesh_index);
			ModelData::Mesh& datas_mesh = data->meshes.at(mesh_index);


			mesh.bone_index = datas_mesh.bone_index;
			mesh.bone_indices.resize(datas_mesh.bone_indices.size());
			{
				for (size_t bone_index = 0; bone_index < mesh.bone_indices.size(); ++bone_index)
				{
					mesh.bone_indices.at(bone_index) = datas_mesh.bone_indices.at(bone_index);
				}
			}

			mesh.subsets.resize(datas_mesh.subsets.size());
			{
				for (size_t subset_index = 0; subset_index < mesh.subsets.size(); ++subset_index)
				{
					ModelResource::Subset& subset	= mesh.subsets[subset_index];
					ModelData::Subset& datas_subset	= datas_mesh.subsets[subset_index];

					subset.index_count = datas_subset.index_count;
					subset.index_start = datas_subset.index_start;

					subset.material = &materials.at(datas_subset.material_index);
				}
			}

			//ボーン変換行列
			mesh.inverse_transforms.resize(datas_mesh.inverse_transforms.size());
			{
				for (size_t transform_index = 0; transform_index < mesh.inverse_transforms.size(); ++transform_index)
				{
					mesh.inverse_transforms.at(transform_index) = datas_mesh.inverse_transforms.at(transform_index);
				}
			}

			//バッファ作成
			mesh.CreateBuffers(device, datas_mesh.vertices.data(), static_cast<unsigned int>(datas_mesh.vertices.size()), datas_mesh.indices.data(), static_cast<unsigned int>(datas_mesh.indices.size()));
		}
	}
}



bool ModelResource::Mesh::CreateBuffers(ID3D11Device* device, ModelData::Vertex* vertices, unsigned int vertex_num, unsigned int* indeces, unsigned int index_num)
{
	HRESULT hr = S_OK;

	
	D3D11_BUFFER_DESC buffer_desc;
	buffer_desc.ByteWidth =sizeof(ModelData::Vertex) * vertex_num;
	buffer_desc.Usage = D3D11_USAGE_IMMUTABLE;
	buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	buffer_desc.CPUAccessFlags = 0;
	buffer_desc.MiscFlags = 0;
	buffer_desc.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA subresource_data;
	subresource_data.pSysMem = vertices;
	subresource_data.SysMemPitch = 0;
	subresource_data.SysMemSlicePitch = 0;
	hr = device->CreateBuffer(&buffer_desc, &subresource_data, vertex_buffer.ReleaseAndGetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("*****************************************************\n");
		OutputDebugStringA("CreateBuffer error(ModelResouce::vertex_buffer)\n");
		OutputDebugStringA("*****************************************************\n");
		assert(!"頂点バッファ作成失敗(ModelResouce)");
	}


	buffer_desc.ByteWidth = sizeof(unsigned int) * index_num;
	buffer_desc.Usage = D3D11_USAGE_IMMUTABLE;
	buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	buffer_desc.CPUAccessFlags = 0;
	buffer_desc.MiscFlags = 0;
	buffer_desc.StructureByteStride = 0;
	subresource_data.pSysMem = indeces;
	subresource_data.SysMemPitch = 0;
	subresource_data.SysMemSlicePitch = 0;
	hr = device->CreateBuffer(&buffer_desc, &subresource_data, index_buffer.ReleaseAndGetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("*****************************************************\n");
		OutputDebugStringA("CreateBuffer error(ModelResouce::index_buffer)\n");
		OutputDebugStringA("*****************************************************\n");
		assert(!"インデックスバッファ作成失敗(ModelResouce)");
	}


	return true;
}

bool ModelResource::Material::CreateShaderResourceView(ID3D11Device* device, std::wstring& texname)
{
	D3D11_TEXTURE2D_DESC texture_desc;

	if (texname.empty())
	{
		HRESULT hr = S_OK;

		ID3D11Texture2D *texture = nullptr;

		texture_desc.Width = 1;
		texture_desc.Height = 1;
		texture_desc.MipLevels = 1;
		texture_desc.ArraySize = 1;
		texture_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		texture_desc.SampleDesc.Count = 1;
		texture_desc.SampleDesc.Quality = 0;
		texture_desc.Usage = D3D11_USAGE_DEFAULT;
		texture_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		texture_desc.CPUAccessFlags = 0;
		texture_desc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA data;
		unsigned char material_color[] = { UCHAR_MAX, UCHAR_MAX, UCHAR_MAX };
		data.pSysMem = material_color;
		data.SysMemPitch = 1;
		data.SysMemSlicePitch = 0;

		hr = device->CreateTexture2D(&texture_desc, &data, &texture);
		if (FAILED(hr))
		{
			OutputDebugStringA("****************************************\n");
			OutputDebugStringA("CreateTexture2D error(SkinnedMesh)\n");
			OutputDebugStringA("****************************************\n");
			assert(!"テクスチャ作成失敗(SkinnedMesh)");
		}

		device->CreateShaderResourceView(texture, nullptr, this->shader_resource_view.GetAddressOf());
		if (!shader_resource_view.Get())
		{
			assert(!"dummy ShaderResourceView Create miss");
		}
		texture->Release();
	}
	else
	{
		if (!ResourceManager::LoadShaderResourceViews(device, texname.c_str(), this->shader_resource_view.GetAddressOf(), &texture_desc))
		{
			OutputDebugStringA("****************************************\n");
			OutputDebugStringA("LoadShaderResourceView error(SkinnedMesh)\n");
			OutputDebugStringA("****************************************\n");
			assert(!"テクスチャ読み込み失敗(SkinnedMesh)");
		}
	}

	return true;
}