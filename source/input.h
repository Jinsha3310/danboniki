#pragma once

#include <Windows.h>
#include <Xinput.h>
#include <memory>
#pragma comment(lib, "xinput9_1_0.lib")

#include "vector.h"

namespace input
{
	enum class KeyLabel
	{
		A = static_cast<int>('A'),
		B = static_cast<int>('B'),
		C = static_cast<int>('C'),
		D = static_cast<int>('D'),
		E = static_cast<int>('E'),
		F = static_cast<int>('F'),
		G = static_cast<int>('G'),
		H = static_cast<int>('H'),
		I = static_cast<int>('I'),
		J = static_cast<int>('J'),
		K = static_cast<int>('K'),
		L = static_cast<int>('L'),
		M = static_cast<int>('M'),
		N = static_cast<int>('N'),
		O = static_cast<int>('O'),
		P = static_cast<int>('P'),
		Q = static_cast<int>('Q'),
		R = static_cast<int>('R'),
		S = static_cast<int>('S'),
		T = static_cast<int>('T'),
		U = static_cast<int>('U'),
		V = static_cast<int>('V'),
		W = static_cast<int>('W'),
		X = static_cast<int>('X'),
		Y = static_cast<int>('Y'),
		Z = static_cast<int>('Z'),

		UP = VK_UP,
		DOWN = VK_DOWN,
		LEFT = VK_LEFT,
		RIGHT = VK_RIGHT,

		TAB = VK_TAB,
		LSHIFT = VK_LSHIFT,
		RSHIFT = VK_RSHIFT,
		LCONTROL = VK_LCONTROL,
		RCONTROL = VK_RCONTROL,

		ESCAPE = VK_ESCAPE,
		SPACE = VK_SPACE,
		BACK = VK_BACK,
		DEL = VK_DELETE,
		ENTER = VK_RETURN,

		NUM1 = VK_NUMPAD1,
		NUM2 = VK_NUMPAD2,
		NUM3 = VK_NUMPAD3,
		NUM4 = VK_NUMPAD4,
		NUM5 = VK_NUMPAD5,
		NUM6 = VK_NUMPAD6,
		NUM7 = VK_NUMPAD7,
		NUM8 = VK_NUMPAD8,
		NUM9 = VK_NUMPAD9,
		NUM0 = VK_NUMPAD0,

		F1 = VK_F1,
		F2 = VK_F2,
		F3 = VK_F3,
		F4 = VK_F4,
		F5 = VK_F5,
		F6 = VK_F6,
		F7 = VK_F7,
		F8 = VK_F8,
		F9 = VK_F9,
		F10 = VK_F10,
		F11 = VK_F11,
		F12 = VK_F12,
	};
	enum class PadLabel
	{
		UP = XINPUT_GAMEPAD_DPAD_UP,
		DOWN = XINPUT_GAMEPAD_DPAD_DOWN,
		LEFT = XINPUT_GAMEPAD_DPAD_LEFT,
		RIGHT = XINPUT_GAMEPAD_DPAD_RIGHT,
		START = XINPUT_GAMEPAD_START,
		BACK = XINPUT_GAMEPAD_BACK,
		LTHUMB = XINPUT_GAMEPAD_LEFT_THUMB,
		RTHUMB = XINPUT_GAMEPAD_RIGHT_THUMB,
		LSHOULDER = XINPUT_GAMEPAD_LEFT_SHOULDER,
		RSHOULDER = XINPUT_GAMEPAD_RIGHT_SHOULDER,
		A = XINPUT_GAMEPAD_A,
		B = XINPUT_GAMEPAD_B,
		X = XINPUT_GAMEPAD_X,
		Y = XINPUT_GAMEPAD_Y,
	};

	enum class MouseLabel
	{
		LEFT_BUTTON = VK_LBUTTON,
		RIGHT_BUTTON = VK_RBUTTON,
		MID_BUTTON = VK_MBUTTON,
	};

	class Keyboard
	{
		int key;
		int current_state;
		int previous_state;

	public:
		/*enum class TRIGGER_MODE {
			NONE,
			RISING_EDGE,
			FALLING_EDGE,
		};*/

		Keyboard(int vkey) : key(vkey), current_state(0), previous_state(0)
		{
		}

		void State();
		bool PressedState() { return current_state > 0; }
		bool RisingState() { return previous_state == 0 && current_state > 0; }
		bool FallingState() { return previous_state > 0 && current_state == 0; }
	};

	class KeyboardManager
	{
	private:
		std::unique_ptr<Keyboard> key[256];
		KeyboardManager()
		{
			for (int i = 0; i < 256; ++i)
			{
				key[i] = std::make_unique<Keyboard>(i);
			}
		}
	public:
		void Update()
		{
			for (int i = 0; i < 256; ++i)
			{
				key[i]->State();
			}
		}
		bool PressedState(const KeyLabel key) { return this->key[static_cast<int>(key)]->PressedState(); }
		bool RisingState(const KeyLabel key) { return this->key[static_cast<int>(key)]->RisingState(); }
		bool FallingState(const KeyLabel key) { return this->key[static_cast<int>(key)]->FallingState(); }
		bool PressedState(const int key) { return this->key[key]->PressedState(); }
		bool RisingState(const int key) { return this->key[key]->RisingState(); }
		bool FallingState(const int key) { return this->key[key]->FallingState(); }

		static KeyboardManager& GetInstance()
		{
			static KeyboardManager instance;
			return instance;
		}
	};


	class GamePad
	{
		struct State {
			XINPUT_STATE state;

			float r_trigger;
			float l_trigger;

			VECTOR2F r_stick;
			VECTOR2F l_stick;

			bool connected = false;
		};

		static constexpr float MAX_STICKTILT = 32767.0f;
		static constexpr float MAX_TRRIGERTILT = 255.0f;
		int id;

		State current_state;
		State previous_state;

		float deadzone_x;
		float deadzone_y;

		void StickState();
		void TriggerState();
		float ApplyDeadZone(float value, const float max_value, const float deadzone);
	public:
		GamePad(const int id, const float deadzone_x, const float deadzone_y);
		~GamePad() {};

		void Update();
		bool ButtonPressedState(const PadLabel pad) { if (this->current_state.state.Gamepad.wButtons & static_cast<int>(pad))return true; return false; }
		bool ButtonRisingState(const PadLabel pad) { if ((this->current_state.state.Gamepad.wButtons & static_cast<int>(pad)) && !(this->previous_state.state.Gamepad.wButtons & static_cast<int>(pad)))return true; return false; }
		bool ButtonFallingState(const PadLabel pad) { if (!(this->current_state.state.Gamepad.wButtons & static_cast<int>(pad)) && (this->previous_state.state.Gamepad.wButtons & static_cast<int>(pad)))return true; return false; }

		float RXStickState() { return current_state.r_stick.x; }
		float RYStickState() { return current_state.r_stick.y; }
		float LXStickState() { return current_state.l_stick.x; }
		float LYStickState() { return current_state.l_stick.y; }

		bool RTriggerPressedState() { return current_state.r_trigger > 0.0f; }
		bool RTriggerRisingState() { return current_state.r_trigger > 0.0f  && previous_state.r_trigger == 0.0f; }
		bool RTriggerFallingState() { return current_state.r_trigger == 0.0f && previous_state.r_trigger > 0.0f; }
		bool LTriggerPressedState() { return current_state.l_trigger > 0.0f; }
		bool LTriggerRisingState() { return current_state.l_trigger > 0.0f  && previous_state.l_trigger == 0.0f; }
		bool LTriggerFallingState() { return current_state.l_trigger == 0.0f && previous_state.l_trigger > 0.0f; }
	};

	class GamePadManager
	{
		std::unique_ptr<GamePad> pad[4];
		GamePadManager() {};
		~GamePadManager() {};
	public:
		void Initialize(const float deadzone_x = 0.1f, const float deadzone_y = 0.1f)
		{
			for (int i = 0; i < 4; ++i)
			{
				pad[i] = std::make_unique<GamePad>(i, deadzone_x, deadzone_y);
			}
		};

		void Update()
		{
			for (int i = 0; i < 4; ++i)
			{
				pad[i]->Update();
			}
		}

		bool ButtonPressedState(const int pad_id, const PadLabel pad) { return this->pad[pad_id]->ButtonPressedState(pad); }
		bool ButtonRisingState(const int pad_id, const PadLabel pad) { return this->pad[pad_id]->ButtonRisingState(pad); }
		bool ButtonFallingState(const int pad_id, const PadLabel pad) { return this->pad[pad_id]->ButtonFallingState(pad); }

		float RXStickState(const int pad_id) { return this->pad[pad_id]->RXStickState(); }
		float RYStickState(const int pad_id) { return this->pad[pad_id]->RYStickState(); }
		float LXStickState(const int pad_id) { return this->pad[pad_id]->LXStickState(); }
		float LYStickState(const int pad_id) { return this->pad[pad_id]->LYStickState(); }

		bool RTriggerPressedState(const int pad_id) { return this->pad[pad_id]->RTriggerPressedState(); }
		bool RTriggerRisingState(const int pad_id) { return this->pad[pad_id]->RTriggerRisingState(); }
		bool RTriggerFallingState(const int pad_id) { return this->pad[pad_id]->RTriggerFallingState(); }
		bool LTriggerPressedState(const int pad_id) { return this->pad[pad_id]->LTriggerPressedState(); }
		bool LTriggerRisingState(const int pad_id) { return this->pad[pad_id]->LTriggerRisingState(); }
		bool LTriggerFallingState(const int pad_id) { return this->pad[pad_id]->LTriggerFallingState(); }

		static GamePadManager& GetInstance()
		{
			static GamePadManager instance;
			return instance;
		}
	};

	class Mouse
	{
		POINT position;
		int buttons[3] = { VK_LBUTTON, VK_RBUTTON, VK_MBUTTON };
		int current_state[3];
		int previous_state[3];

	public:
		int GetCursorPosX() { return static_cast<int>(position.x); };
		int GetCursorPosY() { return static_cast<int>(position.y); };

		void State(HWND hwnd);
		bool PressedState(MouseLabel button);
		bool RisingState(MouseLabel button);
		bool FallingState(MouseLabel button);

		static Mouse& GetInstance()
		{
			static Mouse instance;
			return instance;
		}
	};
};