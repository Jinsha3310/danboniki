#pragma once
#include "vector.h"
#include<DirectXMath.h>
class Enemy;

class Camera
{
	VECTOR3F position;
	VECTOR3F target;
	VECTOR3F lenge;

	enum CAMERA_TYPE
	{
		Relative,TPS,Look_on
	};

	int state;

	Camera() 
	{ 
		position = VECTOR3F(0.f, 0.f, 0.f);
		target = VECTOR3F(0.f, 0.f, 0.f);
		lenge = VECTOR3F(130.f, 70.f, 130.f);
		look_on_flag = false;
		state = Relative; 
	}
	Enemy* enm;
	bool look_on_flag;
public:
	void Update(const VECTOR3F& position, const VECTOR3F& rotation);
	void Relative_move(const VECTOR3F& position);
	void TPS_move(const VECTOR3F& position, const VECTOR3F& rotation);
	void Look_on_move(const VECTOR3F& position);
	void SetLook_no(Enemy*enemy);
	DirectX::XMVECTOR GetCamera_position() { return DirectX::XMVectorSet(position.x, position.y, -position.z, 3.f); }
	DirectX::XMVECTOR GetCamera_target() { return DirectX::XMVectorSet(target.x, target.y, -target.z, 1.f); }
	~Camera();
	static Camera& GetInstance()
	{
		static Camera instance;
		return instance;
	}
};

#define p_Camera (Camera::GetInstance())