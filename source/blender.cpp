#include "blender.h"
#include <assert.h>

ID3D11BlendState*		Blender::blend_state[MODE_MAX] = { 0 };
bool					Blender::load = false;
Blender::BLEND_MODE		Blender::nowMode;


struct _blend_data
{
	D3D11_BLEND		SrcBlend;
	D3D11_BLEND		DestBlend;
	D3D11_BLEND_OP	BlendOp;
	D3D11_BLEND		SrcBlendAlpha;
	D3D11_BLEND		DestBlendAlpha;
	D3D11_BLEND_OP	BlendOpAlpha;

}
blend_data[Blender::MODE_MAX] =
{
	{	//	NONE
		D3D11_BLEND_ONE,				//	SrcBlend
		D3D11_BLEND_ZERO,				//	DestBlend
	D3D11_BLEND_OP_ADD,				//	BlendOp
	D3D11_BLEND_ONE,				//	SrcBlendAlpha
	D3D11_BLEND_ZERO,				//	DestBlendAlpha
	D3D11_BLEND_OP_ADD				//	BlendOpAlpha
	},	//	NONE
	{	//	ALPHA
		D3D11_BLEND_SRC_ALPHA,			//	SrcBlend
		D3D11_BLEND_INV_SRC_ALPHA,		//	DestBlend
	D3D11_BLEND_OP_ADD,				//	BlendOp
	D3D11_BLEND_ONE,				//	SrcBlendAlpha
	D3D11_BLEND_ZERO,				//	DestBlendAlpha
	D3D11_BLEND_OP_ADD				//	BlendOpAlpha
	},	//	ALPHA
	{	//	ADD
		D3D11_BLEND_SRC_ALPHA,			//	SrcBlend
		D3D11_BLEND_ONE,				//	DestBlend
	D3D11_BLEND_OP_ADD,				//	BlendOp
	D3D11_BLEND_ONE,				//	SrcBlendAlpha
	D3D11_BLEND_ZERO,				//	DestBlendAlpha
	D3D11_BLEND_OP_ADD,				//	BlendOpAlpha
	},	//	ADD
	{	//	SUB
		D3D11_BLEND_SRC_ALPHA,			//	SrcBlend
		D3D11_BLEND_ONE,				//	DestBlend
	D3D11_BLEND_OP_REV_SUBTRACT,	//	BlendOp
	D3D11_BLEND_ONE,				//	SrcBlendAlpha
	D3D11_BLEND_ZERO,				//	DestBlendAlpha
	D3D11_BLEND_OP_ADD,				//	BlendOpAlpha
	},	//	SUB
	{	//	REPLACE
		D3D11_BLEND_SRC_ALPHA,			//	SrcBlend
		D3D11_BLEND_ZERO,				//	DestBlend
	D3D11_BLEND_OP_ADD,				//	BlendOp
	D3D11_BLEND_ONE,				//	SrcBlendAlpha
	D3D11_BLEND_ZERO,				//	DestBlendAlpha
	D3D11_BLEND_OP_ADD				//	BlendOpAlpha
	},	//	REPLACE
	{	//	MULTIPLY
		D3D11_BLEND_ZERO,				//	SrcBlend
		D3D11_BLEND_SRC_COLOR,			//	DestBlend
	D3D11_BLEND_OP_ADD,				//	BlendOp
	D3D11_BLEND_DEST_ALPHA,			//	SrcBlendAlpha
	D3D11_BLEND_ZERO,				//	DestBlendAlpha
	D3D11_BLEND_OP_ADD				//	BlendOpAlpha
	},	//	MULTIPLY
	{	//	LIGHTEN
		D3D11_BLEND_ONE,				//	SrcBlend
		D3D11_BLEND_ONE,				//	DestBlend
	D3D11_BLEND_OP_MAX,				//	BlendOp
	D3D11_BLEND_ONE,				//	SrcBlendAlpha
	D3D11_BLEND_ONE,				//	DestBlendAlpha
	D3D11_BLEND_OP_MAX				//	BlendOpAlpha
	},	//	LIGHTEN
	{	//	DARKEN
		D3D11_BLEND_ONE,				//	SrcBlend
		D3D11_BLEND_ONE,				//	DestBlend
	D3D11_BLEND_OP_MIN,				//	BlendOp
	D3D11_BLEND_ONE,				//	SrcBlendAlpha
	D3D11_BLEND_ONE,				//	DestBlendAlpha
	D3D11_BLEND_OP_MIN				//	BlendOpAlpha
	},	//	DARKEN
	{	//	SCREEN
		D3D11_BLEND_SRC_ALPHA,			//	SrcBlend
		D3D11_BLEND_INV_SRC_COLOR,		//	DestBlend
	D3D11_BLEND_OP_ADD,				//	BlendOp
	D3D11_BLEND_ONE,				//	SrcBlendAlpha
	D3D11_BLEND_INV_SRC_ALPHA,		//	DestBlendAlpha
	D3D11_BLEND_OP_ADD				//	BlendOpAlpha
	}	//	SCREEN
};

void Blender::Initialize(ID3D11Device* device, ID3D11DeviceContext* context) 
{
	if (load)
		return;//何度もロードしない

	for (int i = 0; i < MODE_MAX; i++)
	{
		_blend_data& _bd = blend_data[i];

		D3D11_BLEND_DESC bDesc;
		ZeroMemory(&bDesc, sizeof(bDesc));
		bDesc.AlphaToCoverageEnable = FALSE;  // ピクセルをレンダーターゲットに設定するときに、マルチサンプリング手法としてalpha-to-coverageを使用するかどうかを指定します
		bDesc.IndependentBlendEnable = FALSE; // 同時レンダリングターゲットで独立したブレンドを有効にするかどうかを指定します。
											  // 独立した混合を有効にするには、TRUEに設定します。FALSEに設定すると、RenderTarget[0]メンバのみが使用されます。

		D3D11_RENDER_TARGET_BLEND_DESC &rtbd = bDesc.RenderTarget[0];
		rtbd.BlendEnable = (i != NONE ? TRUE : FALSE);				// ブレンディングを有効（または無効）にする。
		rtbd.SrcBlend = _bd.SrcBlend;								// ピクセルシェーダーが出力するRGB値に対して実行する操作を指定する。
		rtbd.DestBlend = _bd.DestBlend;								// レンダーターゲット内の現在のRGB値に対して実行する操作を指定する。
		rtbd.BlendOp = _bd.BlendOp;									// どのように組み合わせるか定義し、SrcBlendとDestBlend動作を制御する。
		rtbd.SrcBlendAlpha = _bd.SrcBlendAlpha;						// ピクセルシェーダーが出力するアルファ値に対して実行する操作を指定する。
		rtbd.DestBlendAlpha = _bd.DestBlendAlpha;					// レンダーターゲットの現在のアルファ値に対して実行する操作を指定する。
		rtbd.BlendOpAlpha = _bd.BlendOpAlpha;						// どのように組み合わせるかを定義しSrcBlendAlphaとDestBlendAlpha動作を制御する。
		rtbd.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;  // 書き込みのマスク
		if (FAILED(device->CreateBlendState(&bDesc, &blend_state[i])))
		{
			assert("blend state create error");
		}
		
		nowMode = MODE_MAX;
		load = true;
	}
}

void	Blender::Set(BLEND_MODE mode, ID3D11DeviceContext* context)
{
	//	エラーチェック
	if (!load)							return;
	if (mode < 0 || mode >= MODE_MAX)	return;
	if (mode == nowMode)				return;

	//	ブレンドの設定
	context->OMSetBlendState(blend_state[mode], nullptr, 0xFFFFFFFF);
	nowMode = mode;
}

void Blender::Release()
{
	for (auto &p : blend_state)
	{
		if (p)		p->Release();
		p = nullptr;
	}

	nowMode = MODE_MAX;				//	異常値(未設定状態)
	load = false;
}