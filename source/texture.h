#pragma once
#include <memory>
#include "sprite.h"

namespace texture
{
	class Texture
	{
		SpriteBatch*    sprBatch;
	public:
		Texture();
		~Texture();

		bool LoadTexture(const wchar_t* filename, unsigned int maxinstance);
		void Release();

		void Begin(ID3D11DeviceContext* device_context);
		void End(ID3D11DeviceContext* device_context);

		void Render(
			const VECTOR2F& pos, const VECTOR2F& scale,
			const VECTOR2F& tex_pos, const VECTOR2F& tex_size,
			const VECTOR2F& center,
			const float angle,
			const VECTOR4F& colour, bool reverse = false);
		bool isEmpty();
	};
	constexpr unsigned int MAXINSTANCE = 1U;
	class TextureManager
	{
		int handle_num;
		std::map<int, std::unique_ptr<Texture>> texture_data;

	public:
		TextureManager() : handle_num(0) { texture_data.clear(); };
		~TextureManager() { handle_num = 0; texture_data.clear(); };

		int LoadTexture(const wchar_t* filename, unsigned int maxinstance = MAXINSTANCE);
		void Begin(int num);
		void End(int num);

		void Render(int num, const VECTOR2F& position, const VECTOR2F& scale,
			const VECTOR2F& texPos, const VECTOR2F& texSize,
			const VECTOR2F& center,
			const float angle,
			const VECTOR4F& colour,
			bool reverse = false);


		static TextureManager& GetInstance()
		{
			static TextureManager instance;
			return instance;
		}
	};
}