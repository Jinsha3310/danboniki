#include "depth_stencil.h"

ID3D11DepthStencilState*    DepthStencilState::states[DepthStencilState::DEPTH_STENCIL_STATE::MAX] = { 0 };
DepthStencilState::DEPTH_STENCIL_STATE    DepthStencilState::nowMode = DepthStencilState::DEPTH_STENCIL_STATE::MAX;

void DepthStencilState::Initialize(ID3D11Device* device)
{
	HRESULT hr = S_OK;

	D3D11_DEPTH_STENCIL_DESC dsDesc;

	// 深度＆ステンシルテストなし設定
	dsDesc.DepthEnable					= false;						// 深度テストを有効
	dsDesc.DepthWriteMask				= D3D11_DEPTH_WRITE_MASK_ZERO;	// 深度データによって変更できる深度ステンシルバッファの部分を識別する。
	dsDesc.DepthFunc					= D3D11_COMPARISON_ALWAYS;		// 深度データを既存の深度データと比較する。
	dsDesc.StencilEnable				= false;						// ステンシルテストを有効
	dsDesc.StencilReadMask				= 0xFF;							// ステンシルデータを読み取るための深度ステンシルバッファを指定。
	dsDesc.StencilWriteMask				= 0xFF;							// ステンシルデータを書き込むための深度ステンシルバッファを識別。
	// FrontFace サーフェス法線がカメラの方を向いているピクセルについて、深度テストとステンシルテストの結果を使用する方法を特定																// FrontFace サーフェス法線がカメラの方を向いているピクセルについて、深度テストとステンシルテストの結果を使用する方法を特定
	dsDesc.FrontFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;		// ステンシルテストが失敗した時に実行するステンシル操作。
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;		// ステンシルテストが成功し、深度テストが失敗した時に実行するステンシル操作。
	dsDesc.FrontFace.StencilPassOp		= D3D11_STENCIL_OP_KEEP;		// ステンシルテストと深度テストの両方が成功した時に実行するステンシル操作。
	dsDesc.FrontFace.StencilFunc		= D3D11_COMPARISON_ALWAYS;		// ステンシルデータを既存のステンシルデータと比較する機能
	// BackFace  サーフェス法線がカメラと反対方向を向いているピクセルについて、深度テストとステンシルテストの結果を使用する方法を特定。
	dsDesc.BackFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp	= D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp		= D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;
	hr = device->CreateDepthStencilState(&dsDesc, &states[NONE]);
	if (FAILED(hr))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("depth stencil state create error\n");
		OutputDebugStringA("***************************************\n");
		assert("Create Depth Stencil State error");
	}
	// 深度テストが失敗したときにステンシル値を書き込む設定
	// このステートで描画したときは深度テストに必ず失敗するようにしているため、
	// ピクセルに色は打たれないが、ステンシルバッファにステンシル値が書き込まれる。
	// 000000000000
	// 000011110000  ←描画した場所のステンシルバッファに値が書き込まる。
	// 000011110000
	// 000000000000
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dsDesc.DepthFunc = D3D11_COMPARISON_NEVER;	// 深度テストを必ず失敗させる。（描画はしないがステンシル値は書き込みしたいため）
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_REPLACE;	// ステンシルテストに合格し、深度テストが失敗した時に
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;	// 必ずステンシルテストは合格させる。
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_REPLACE;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	hr = device->CreateDepthStencilState(&dsDesc, &states[MASK]);
	if (FAILED(hr))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("depth stencil state create error\n");
		OutputDebugStringA("***************************************\n");
		assert("Create Depth Stencil State error");
	}

	// 深度バッファに書き込まれている値と同じ値を参照した場合はピクセルに色を打たないようにする。
	// 000000000000
	// 000011110000  ← 1の場所に描画しようとした場合、その場所はピクセルが打たれない。
	// 000011110000
	// 000000000000
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dsDesc.DepthFunc = D3D11_COMPARISON_ALWAYS;
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;		// ステンシルバッファに値は書き込まない
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;//
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;		//
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_NOT_EQUAL;	// ステンシルバッファの値と同じ値が参照された場合はテストに失敗する。
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_NOT_EQUAL;
	hr = device->CreateDepthStencilState(&dsDesc, &states[DRAW]);
	if (FAILED(hr))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("depth stencil state create error\n");
		OutputDebugStringA("***************************************\n");
		assert("Create Depth Stencil State error");
	}

	nowMode = DEPTH_STENCIL_STATE::MAX;
}