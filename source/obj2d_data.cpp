#include "obj2d_data.h"
#include "framework.h"

//TextureData::TextureData() :animeframe(nullptr), animedata(nullptr)
//{
//
//}
//
//TextureData::~TextureData()
//{
//	
//}
//
//bool TextureData::Animetion(const float elapsed_time)
//{
//	if (!animedata)
//		return false;
//
//	//アニメーションデータが切り替わった時、最初から再生する
//	if (animework.previous_state != animedata) {
//		animework.previous_state = animedata;
//		animework.num = 0;
//		animework.frame = 0;
//	}
//
//	animedata += animework.num;
//	animeframe = animedata->data;	//現在のパターン番号に該当する画像をセット
//
//	animework.frame += elapsed_time * 60.f;
//	if (animework.frame >= animedata->frame) {	//設定フレーム数表示したら
//		animework.frame = 0;
//		animework.num++;						//次のパターンへ
//		if ((animedata + 1)->frame < 0) {	//終了コードのとき
//			animework.num = 0;				//先頭へ戻る
//			return true;
//		}
//	}
//
//	return false;
//}
//
//void TextureData::Render(ID3D11DeviceContext* context,
//	const VECTOR2F& position, const VECTOR2F& scale,
//	const float angle,
//	const VECTOR4F& color)
//{
//	VECTOR2F tex_pos (GetAnimeFrameLeft(),    GetAnimeFrameTop());
//	VECTOR2F tex_size(GetAnimeFrameWidth(),   GetAnimeFrameHeight());
//	VECTOR2F center  (GetAnimeFrameCenterX(), GetAnimeFrameCenterY());
//
//	texture::TextureManager::getInstance()->Begin(context, this->texNo);
//	texture::TextureManager::getInstance()->Render(this->texNo, position, scale, tex_pos, tex_size, center, angle, color);
//	texture::TextureManager::getInstance()->End(context, this->texNo);
//}

bool AnimeData::Animetion(const float elapsed_time, const float cnt_max, const int frame_max)
{
	animecnt += elapsed_time * 60.f;

	if (animecnt > cnt_max)
	{
		animecnt = 0;
		++animeframe;

		if (animeframe >= frame_max)
		{
			animeframe = 0;
		}
	}

	return true;
}