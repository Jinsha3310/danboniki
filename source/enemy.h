#pragma once
#include "skinned_mesh.h"
#include "vector.h"

#include <memory>

class Enemy
{
	enum ANIMATION_CLIP { CLIP_IDLE, CLIP_MOVE };

	ANIMATION_CLIP	animation_clip = CLIP_IDLE;

	bool IsPlayAnimation() const { return substance->model->IsPlayAnimation(); }
	void PlayAnimation(ANIMATION_CLIP animation_index);
	void Animate(const float elapsed_time);
public:
	std::unique_ptr<SkinnedMesh> substance;

	VECTOR3F scale;
	VECTOR3F rotate;
	VECTOR3F velocity;
	VECTOR3F position;

	bool exist;

	enum STATES { STATE_IDLE, STATE_MOVE };
	STATES state = STATE_IDLE;

	bool Initialize();
	bool Update(const float elapsed_time);
	void Render(ID3D11DeviceContext* context, const DirectX::XMFLOAT4X4& view_projection, const VECTOR4F& light, const VECTOR4F& color, bool modeling_method)
	{
		substance->Render(context, view_projection, light, color, modeling_method);
	}
};