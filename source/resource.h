#pragma once
#include <D3D11.h>

#include <map>
#include <wrl.h>


class ResourceManager
{
	struct VertexShader_and_InputLayout
	{
		VertexShader_and_InputLayout(ID3D11VertexShader  *vertex_shader, ID3D11InputLayout *input_layout) : vertex_shader(vertex_shader), input_layout(input_layout) {}

		Microsoft::WRL::ComPtr<ID3D11VertexShader> vertex_shader;
		Microsoft::WRL::ComPtr<ID3D11InputLayout> input_layout;
	};

	static std::map<std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>	textures;
	static std::map<std::string, VertexShader_and_InputLayout> 					vertex_shaders;
	static std::map<std::string,  Microsoft::WRL::ComPtr<ID3D11PixelShader>> 		pixel_shaders;

	
	ResourceManager() {}
	~ResourceManager() {}
public :
	static void Release()
	{
		textures.clear();
		vertex_shaders.clear();
		pixel_shaders.clear();
	}

	static bool LoadShaderResourceViews(ID3D11Device* device, const wchar_t* filename,
		ID3D11ShaderResourceView** srv, D3D11_TEXTURE2D_DESC* texDesc);
	static void ReleaseShaderResourceViews(std::wstring wstr);

	static bool LoadVertexShaders(ID3D11Device* device, const char* filename,
		D3D11_INPUT_ELEMENT_DESC *elementDescs, int numElement,
		ID3D11VertexShader** vs, ID3D11InputLayout** il);
	static void ReleaseVertexShaders(std::string str);

	static bool LoadPixelShaders(ID3D11Device* device, const char* filename, ID3D11PixelShader** ps);
	static void ReleasePixelShaders(std::string str);
};