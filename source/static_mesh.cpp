#include "static_mesh.h"
#include "resource.h"

#include <iostream>
#include <fstream>
#include <shlwapi.h>

StaticMesh::StaticMesh(ID3D11Device* device, const char* filename, bool flipping_v_coodinates, const char* extension)
{
	HRESULT hr = S_OK;

	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,	0, 0,							  D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT,	0, D3D11_APPEND_ALIGNED_ELEMENT,  D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,		0, D3D11_APPEND_ALIGNED_ELEMENT,  D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	UINT num_elements = ARRAYSIZE(layout);

	if (!ResourceManager::LoadVertexShaders(device, "./Data/Shaders/static_mesh_vs.cso", layout, num_elements, vertex_shader.GetAddressOf(), input_layout.GetAddressOf())) {
		OutputDebugStringA("*********************************************\n");
		OutputDebugStringA("LoadVertexShaders error(StaticMesh)\n");
		OutputDebugStringA("*********************************************\n");

		assert(!"頂点シェーダ作成失敗(StaticMesh)");
	}

	if (!ResourceManager::LoadPixelShaders(device, "./Data/Shaders/static_mesh_ps.cso", pixel_shader.GetAddressOf())) 
	{
		OutputDebugStringA("*********************************************\n");
		OutputDebugStringA("LoadPixelShaders error(StaticMesh)\n");
		OutputDebugStringA("*********************************************\n");

		assert(!"ピクセルシェーダ作成失敗(StaticMesh)");
	}

	//ラスタライザステート ソリッド描画
	D3D11_RASTERIZER_DESC rasterizer_desc_solid;			// 
	rasterizer_desc_solid.FillMode = D3D11_FILL_SOLID;
	rasterizer_desc_solid.CullMode = D3D11_CULL_BACK;
	rasterizer_desc_solid.FrontCounterClockwise = false;
	rasterizer_desc_solid.DepthBias = 0;
	rasterizer_desc_solid.DepthBiasClamp = 0.0f;
	rasterizer_desc_solid.SlopeScaledDepthBias = 0.0f;
	rasterizer_desc_solid.DepthClipEnable = true;
	rasterizer_desc_solid.MultisampleEnable = false;
	rasterizer_desc_solid.ScissorEnable = false;
	rasterizer_desc_solid.AntialiasedLineEnable = false;
	hr = device->CreateRasterizerState(&rasterizer_desc_solid, solid_state.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateRasterizerState error(StaticMesh::solid)\n");
		OutputDebugStringA("***************************************\n");
		assert(!"ラスタライザ作成失敗(StaticMesh::solid)");
	}

	//ラスタライザステート ワイヤー描画
	D3D11_RASTERIZER_DESC rasterizer_desc_wireframe;
	rasterizer_desc_wireframe.FillMode = D3D11_FILL_WIREFRAME;
	rasterizer_desc_wireframe.CullMode = D3D11_CULL_BACK;
	rasterizer_desc_wireframe.FrontCounterClockwise = false;
	rasterizer_desc_wireframe.DepthBias = 0;
	rasterizer_desc_wireframe.DepthBiasClamp = 0.0f;
	rasterizer_desc_wireframe.SlopeScaledDepthBias = 0.0f;
	rasterizer_desc_wireframe.DepthClipEnable = true;
	rasterizer_desc_wireframe.MultisampleEnable = false;
	rasterizer_desc_wireframe.ScissorEnable = false;
	rasterizer_desc_wireframe.AntialiasedLineEnable = true;
	hr = device->CreateRasterizerState(&rasterizer_desc_wireframe, wireframe_state.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("**************************************************\n");
		OutputDebugStringA("CreateRasterizerState error(StaticMesh::wireframe)\n");
		OutputDebugStringA("**************************************************\n");
		assert(!"ラスタライザ作成失敗(StaticMesh::wireframe)");
	}

	D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
	depth_stencil_desc.DepthEnable = true;
	depth_stencil_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depth_stencil_desc.DepthFunc = D3D11_COMPARISON_LESS;
	depth_stencil_desc.StencilEnable = false;
	depth_stencil_desc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
	depth_stencil_desc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
	depth_stencil_desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depth_stencil_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	hr = device->CreateDepthStencilState(&depth_stencil_desc, depth_stencil_state.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("*****************************************\n");
		OutputDebugStringA("CreateDepthStencilState error(StaticMesh)\n");
		OutputDebugStringA("*****************************************\n");
		assert(!"深度ステンシル作成失敗(StaticMesh)");
	}
#if(1)
	if(PathFileExistsA((std::string(filename) + extension).c_str()))
	{
		std::ifstream ifs;
		ifs.open((std::string(filename) + extension).c_str(), std::ios::binary);
		cereal::BinaryInputArchive i_archive(ifs);
		i_archive(*this);
	}
	else
	{
		wchar_t w_filename[256];

		mbstowcs_s(0, w_filename, filename, strlen(filename) + 1);
		
		if (!LoadOBJFile(device, w_filename, flipping_v_coodinates))
		{
			OutputDebugStringA("*****************************\n");
			OutputDebugStringA("LoadOBJFile error(StaticMesh)\n");
			OutputDebugStringA("*****************************\n");
			assert(!"OBJ作成失敗(StaticMesh)");
		}

		std::ofstream ofs;
		ofs.open((std::string(filename) + extension).c_str(), std::ios::binary);
		cereal::BinaryOutputArchive o_archive(ofs);
		o_archive(*this);
	}
#else
	wchar_t w_filename[256];

	mbstowcs_s(0, w_filename, filename, strlen(filename) + 1);
	if (!LoadOBJFile(device, w_filename, flipping_v_coodinates))
	{
		OutputDebugStringA("*************************************************\n");
		OutputDebugStringA("LoadOBJFile error(StaticMesh)\n");
		OutputDebugStringA("*************************************************\n");
		assert(!"OBJ作成失敗(StaticMesh)");
	}
#endif
	CreateBuffer(device, vertices.data(), static_cast<unsigned int>(vertices.size()), indices.data(), static_cast<unsigned int>(indices.size()));


	for (Material& material : materials)
	{
		if (!ResourceManager::LoadShaderResourceViews(device, material.diffuse_map_name.c_str(), material.shader_resource_view.GetAddressOf(), &texture_desc)) {
			OutputDebugStringA("****************************************\n");
			OutputDebugStringA("LoadShaderResourceView error(StaticMesh)\n");
			OutputDebugStringA("****************************************\n");
			assert(!"テクスチャ読み込み失敗(StaticMesh)");
		}
	}

	D3D11_SAMPLER_DESC samplerDesc = {};
	samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0;
	samplerDesc.MaxAnisotropy = 16;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 1.0f;
	samplerDesc.BorderColor[1] = 1.0f;
	samplerDesc.BorderColor[2] = 1.0f;
	samplerDesc.BorderColor[3] = 1.0f;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = device->CreateSamplerState(&samplerDesc, sampler_state.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateSamplerState error(StaticMesh)\n");
		OutputDebugStringA("***************************************\n");

		assert(!"サンプラーステート作成失敗(StaticMesh)");
	}
}

bool StaticMesh::LoadOBJFile(ID3D11Device* device, const wchar_t* filename, bool flipping_v_coodinates)
{
	std::vector<std::wstring> mtl_filenames;

	std::wifstream fin(filename);
	_ASSERT_EXPR(fin, L"OBJ file not found");

	wchar_t command[256];
	while (fin) {
		fin >> command;
		//if (0 == wcscmp(command, L"#")) {
		//	//コメント文
		//	fin.ignore(1024, L'\n');
		//}
		if (0 == wcscmp(command, L"mtllib")) {
			fin.ignore();
			//mtlファイルの名前
			wchar_t mtl_lib[256];
			fin >> mtl_lib;
			mtl_filenames.push_back(mtl_lib);
		}
		else if (0 == wcscmp(command, L"usemtl")) {
			wchar_t usemtl[MAX_PATH];
			fin >> usemtl;
			Subset current_subset = {};
			current_subset.usemtl = usemtl;
			current_subset.index_start = indices.size();
			subsets.push_back(current_subset);
		}
		else if (0 == wcscmp(command, L"v")) {
			float x, y, z;
			fin >> x >> y >> z;
			positions.push_back(VECTOR3F(x, y, z));
			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"vt")) {
			float u, v;
			fin >> u >> v;
			if (flipping_v_coodinates)
				texcoords.push_back(VECTOR2F(u, 1.0f - v));
			else
				texcoords.push_back(VECTOR2F(u, v));
			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"vn")) {
			float i, j, k;
			fin >> i >> j >> k;
			normals.push_back(VECTOR3F(i, j, k));
			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"f")) {
			static UINT index = 0;
			for (UINT i = 0; i < 3; i++) {
				Vertex vertex;
				UINT v, vt, vn;

				fin >> v;
				vertex.position = positions[v - 1];
				if (L'/' == fin.peek()) {
					fin.ignore();
					if (L'/' != fin.peek()) {
						fin >> vt;
						vertex.texcoord = texcoords[vt - 1];
					}
					if (L'/' == fin.peek()) {
						fin.ignore();
						fin >> vn;
						vertex.normal = normals[vn - 1];
					}
				}
				vertices.push_back(vertex);
				indices.push_back(current_index++);
			}
			fin.ignore(1024, L'\n');
		}
		else {
			fin.ignore(1024, L'\n');
		}
	}
	fin.close();

	auto&& it = subsets.rbegin();
	it->index_count = indices.size() - it->index_start;
	for (it = subsets.rbegin() + 1; it != subsets.rend(); ++it) {
		it->index_count = (it - 1)->index_start - it->index_start;
	}

	wchar_t mtl_filename[OBJ_NAME_LENGTH];
	GetResourcePath(mtl_filename, filename, mtl_filenames[0].c_str());

	//if (0 != wcscmp(mtl_filename, mtl_filenames[0].c_str()))
	{
		if (!LoadMTLFile(mtl_filename))
		{
			OutputDebugStringA("*************************************************\n");
			OutputDebugStringA("LoadMTLFile error(StaticMesh)\n");
			OutputDebugStringA("*************************************************\n");
			assert(!"MTL作成失敗(StaticMesh)");
		}
	}

	return true;
}

bool StaticMesh::LoadMTLFile(const wchar_t* filename)
{
	int mtl_cnt = 0;

	std::wifstream fin(filename);
	if (!fin)
	{
		OutputDebugStringA("****************************************************\n");
		OutputDebugStringA("マテリアルのファイルオープンに失敗 error(StaticMesh)\n");
		OutputDebugStringA("****************************************************\n");
		return false;
	}


	wchar_t command[256];
	while (fin)
	{
		fin >> command;
		if (0 == wcscmp(command, L"#")) {
			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"newmtl"))
		{
			wchar_t newmtl[OBJ_NAME_LENGTH] = { 0 };
			
			fin >> newmtl;

			Material material;
			material.newmtl_name = newmtl;
			
			materials.push_back(material);

			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"Ns"))
		{
			fin >> materials.rbegin()->shininess;
			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"Tr"))
		{
			fin >> materials.rbegin()->alpha;
			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"Kd"))
		{
			float r, g, b;
			fin >> r >> g >> b;
			materials.rbegin()->diffuse = VECTOR3F(r, g, b);
			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"Ks"))
		{
			float r, g, b;
			fin >> r >> g >> b;
			materials.rbegin()->specular = VECTOR3F(r, g, b);
			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"Ka"))
		{
			float r, g, b;
			fin >> r >> g >> b;
			materials.rbegin()->ambient = VECTOR3F(r, g, b);
			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"map_Kd"))
		{
			wchar_t map_Kd[OBJ_NAME_LENGTH] = { 0 };

			fin >> map_Kd;
			GetResourcePath(map_Kd, filename, map_Kd);
			materials.rbegin()->diffuse_map_name = map_Kd;

			fin.ignore(1024, L'\n');
		}
		else if (0 == wcscmp(command, L"illum")) {
			unsigned int illum;
			fin >> illum;
			materials.rbegin()->illum = illum;
			fin.ignore(1024, L'\n');
		}
		else {
			fin.ignore(1024, L'\n');
		}
	}
	fin.close();


	return true;
}


bool StaticMesh::CreateBuffer(ID3D11Device* device, Vertex* vertices, int vertex_num, unsigned int* indeces, int index_num)
{
	HRESULT hr = S_OK;

	D3D11_BUFFER_DESC buffer_desc;
	buffer_desc.ByteWidth = sizeof(Vertex) * vertex_num;
	buffer_desc.Usage = D3D11_USAGE_IMMUTABLE;
	buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	buffer_desc.CPUAccessFlags = 0;
	buffer_desc.MiscFlags = 0;
	buffer_desc.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA subresource_data;
	subresource_data.pSysMem = vertices;
	subresource_data.SysMemPitch = 0;
	subresource_data.SysMemSlicePitch = 0;
	hr = device->CreateBuffer(&buffer_desc, &subresource_data, vertex_buffer.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("*****************************************************\n");
		OutputDebugStringA("CreateBuffer error(GeometricPrimitive::vertex_buffer)\n");
		OutputDebugStringA("*****************************************************\n");
		assert(!"頂点バッファ作成失敗(GeometricPrimitive)");
	}

	if (indeces) {
		buffer_desc.ByteWidth = sizeof(unsigned int) * index_num;
		buffer_desc.Usage = D3D11_USAGE_IMMUTABLE;
		buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		buffer_desc.CPUAccessFlags = 0;
		buffer_desc.MiscFlags = 0;
		buffer_desc.StructureByteStride = 0;
		subresource_data.pSysMem = indeces;
		subresource_data.SysMemPitch = 0;
		subresource_data.SysMemSlicePitch = 0;
		hr = device->CreateBuffer(&buffer_desc, &subresource_data, index_buffer.GetAddressOf());
		if (FAILED(hr))
		{
			OutputDebugStringA("*****************************************************\n");
			OutputDebugStringA("CreateBuffer error(GeometricPrimitive::index_buffer)\n");
			OutputDebugStringA("*****************************************************\n");
			assert(!"インデックスバッファ作成失敗(GeometricPrimitive)");
		}
	}

	buffer_desc.ByteWidth = sizeof(CbScene);
	buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	buffer_desc.CPUAccessFlags = 0;
	buffer_desc.MiscFlags = 0;
	buffer_desc.StructureByteStride = 0;
	hr = device->CreateBuffer(&buffer_desc, nullptr, cb_scene.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("********************************************************\n");
		OutputDebugStringA("CreateBuffer error(GeometricPrimitive::constant_buffer)\n");
		OutputDebugStringA("********************************************************\n");
		assert(!"定数バッファ作成失敗(GeometricPrimitive)");
	}

	buffer_desc.ByteWidth = sizeof(CbMesh);
	hr = device->CreateBuffer(&buffer_desc, nullptr, cb_mesh.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("********************************************************\n");
		OutputDebugStringA("CreateBuffer error(GeometricPrimitive::constant_buffer)\n");
		OutputDebugStringA("********************************************************\n");
		assert(!"定数バッファ作成失敗(GeometricPrimitive)");
	}

	buffer_desc.ByteWidth = sizeof(CbSubset);
	hr = device->CreateBuffer(&buffer_desc, nullptr, cb_subset.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("********************************************************\n");
		OutputDebugStringA("CreateBuffer error(GeometricPrimitive::constant_buffer)\n");
		OutputDebugStringA("********************************************************\n");
		assert(!"定数バッファ作成失敗(GeometricPrimitive)");
	}

	return true;
}

void StaticMesh::Render(ID3D11DeviceContext* context,
	const DirectX::XMFLOAT4X4& view_projection,
	const DirectX::XMFLOAT4X4& world,
	const VECTOR4F& light,
	const VECTOR4F& color,
	bool            color_in)
{
	UINT strides = sizeof(Vertex);
	UINT offsets = 0;

	context->IASetVertexBuffers(0, 1, vertex_buffer.GetAddressOf(), &strides, &offsets);
	context->IASetIndexBuffer(index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetInputLayout(input_layout.Get());

	if (color_in) context->RSSetState(solid_state.Get());
	else		  context->RSSetState(wireframe_state.Get());

	context->VSSetShader(vertex_shader.Get(), nullptr, 0);
	context->PSSetShader(pixel_shader.Get(), nullptr, 0);
	context->OMSetDepthStencilState(depth_stencil_state.Get(), 0);

	ID3D11Buffer* constant_buffers[] = {
		cb_scene.Get(),
		cb_mesh.Get(),
		cb_subset.Get(),
	};

	context->VSSetConstantBuffers(0, ARRAYSIZE(constant_buffers), constant_buffers);
	context->PSSetConstantBuffers(0, ARRAYSIZE(constant_buffers), constant_buffers);

	CbScene scene_data;
	scene_data.view_projection = view_projection;
	scene_data.light_direction = light;
	context->UpdateSubresource(cb_scene.Get(), 0, nullptr, &scene_data, 0, 0);

	for (Material& material : materials)
	{
		context->PSSetShaderResources(0, 1, material.shader_resource_view.GetAddressOf());
		context->PSSetSamplers(0, 1, sampler_state.GetAddressOf());

		CbMesh mesh_data;
		mesh_data.world = world;
		context->UpdateSubresource(cb_mesh.Get(), 0, nullptr, &mesh_data, 0, 0);

		CbSubset subset_data;
		subset_data.material_color = VECTOR4F(color.x*material.diffuse.x, color.y*material.diffuse.y, color.z*material.diffuse.z, color.w);
		context->UpdateSubresource(cb_subset.Get(), 0, nullptr, &subset_data, 0, 0);

		for (Subset& subset : subsets) {
			
			if (material.newmtl_name == subset.usemtl) {
				context->DrawIndexed(static_cast<unsigned int>(subset.index_count), static_cast<unsigned int>(subset.index_start), 0);
			}
		}
	}
}