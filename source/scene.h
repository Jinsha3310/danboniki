#pragma once

#include <d3d11.h>

class Scene
{
public:
	virtual bool Initialize(ID3D11Device *device) = 0;
	virtual const char* Update(float elapsed_time) = 0;
	virtual void Render(ID3D11DeviceContext* context) = 0;
	virtual void Finalize() = 0;

	Scene() = default;
	virtual ~Scene() = default;
	Scene(Scene&) = delete;
	void operator=(Scene&) = delete;
};