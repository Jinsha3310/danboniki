#include "framework.h"
#include "scene_game.h"
#include "fbx_loader.h"
#include "camera.h"
#include "player.h"
#include "enemy_manager.h"

bool SceneGame::Initialize(ID3D11Device *device)
{
	loading_thread = std::make_unique<std::thread>([&](ID3D11Device* device)
	{
		std::lock_guard<std::mutex> lock(loading_mutex);

		if (!Player::GetInstance().loaded)
		{
			pPlayer.substances.emplace_back();
			pPlayer.substances.back() = std::make_unique<SkinnedMesh>(device, "./DATA/FBX/danbo_fbx/danbo_taiki.fbx");
			pPlayer.substances.emplace_back();
			pPlayer.substances.back() = std::make_unique<SkinnedMesh>(device, "./DATA/FBX/danbo_fbx/danbo_taiki_idou.fbx");
			pPlayer.substances.emplace_back();
			pPlayer.substances.back() = std::make_unique<SkinnedMesh>(device, "./DATA/FBX/danbo_fbx/danbo_atk.fbx");
			pPlayer.substances.emplace_back();
			pPlayer.substances.back() = std::make_unique<SkinnedMesh>(device, "./DATA/FBX/danbo_fbx/danbo_fly.fbx");
			
			pPlayer.loaded = true;
		}

		
		if (!EnemyManager::GetInstance(device, "./DATA/FBX/Zombie Walk.fbx").loaded)
		{
			pEnemyManager.loaded = true;
		}
		pEnemyManager.Initialize();

		
	}, device);
	



	return true;
}

const char* SceneGame::Update(float elapsed_time)
{
	if (IsNowLoading())
	{
		return 0;
	}
	EndLoading();

	pPlayer.Update(elapsed_time);
	pEnemyManager.Update(elapsed_time);
	p_Camera.Update(pPlayer.position, pPlayer.rotate);
	
	return 0;
}

void SceneGame::Render(ID3D11DeviceContext* context)
{
	if (IsNowLoading())
	{
		FrameWork::font->DrawString(context, 500, 500, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, "Now Loading ...");
		
		return;
	}
	//EndLoading();
	VECTOR4F color(1.0f, 1.0f, 1.0f, 1.0f);
	light = VECTOR4F(0.0f, 0.0f, -1.0f, 0.0f);

	DirectX::XMMATRIX V;//�r���[�s��  ���[���h�s����t�s��ł����߂���
	{
		DirectX::XMVECTOR pos, target, upward = { 0.0f, 1.0f, 0.0f, 0.0f };
		pos = p_Camera.GetCamera_position();
		target = p_Camera.GetCamera_target();

		V = DirectX::XMMatrixLookAtLH(pos, target, upward);
	}
	DirectX::XMMATRIX P;//�v���W�F�N�V�����s��
	{
		D3D11_VIEWPORT viewport;
		unsigned int num_viewport = 1;
		context->RSGetViewports(&num_viewport, &viewport);

		//�������e
		P = DirectX::XMMatrixPerspectiveFovLH(30.f * 0.01745f, viewport.Width / viewport.Height, 0.1f, 1000.f);
		//���s���e
		//P = DirectX::XMMatrixOrthographicLH(static_cast<float>(SCREEN_WIDTH), static_cast<float>(SCREEN_HEIGHT), 0.1f, 1000.f);
	}
	DirectX::XMMATRIX C = DirectX::XMMatrixSet(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, -1, 0,
		0, 0, 0, 1
	);
	
	DirectX::XMFLOAT4X4 view_projection;

	DirectX::XMStoreFloat4x4(&view_projection, C*V*P);

	pPlayer.Render(context, view_projection, light, color, true);
	pEnemyManager.Render(context, view_projection, light, color, true);
	//FrameWork::font->DrawString(context, 500, 500, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, "GAME");
}

void SceneGame::Finalize()
{
	if (loading_thread && loading_thread->joinable())
	{
		loading_thread->join();
	}
}