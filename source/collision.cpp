#include "collision.h"


/*/2D����/*/
//�����̓����蔻��
bool isHitSegment(const VECTOR2F& A, const VECTOR2F& B, const VECTOR2F& C, const VECTOR2F& D)
{
	VECTOR2F vec_AB, vec_AC, vec_AD;
	vec_AB = B - A;
	vec_AC = C - A;
	vec_AD = D - A;

	VECTOR2F vec_CA, vec_CB, vec_CD;
	vec_CA = A - C;
	vec_CB = B - C;
	vec_CD = D - C;

	float v1 = CrossVec2(vec_AB, vec_AC);
	float v2 = CrossVec2(vec_AB, vec_AD);
	float v3 = CrossVec2(vec_CD, vec_CA);
	float v4 = CrossVec2(vec_CD, vec_CB);

	if (v1 * v2 <= 0.0f && v3 * v4 <= 0.0f)
		return true;

	return false;
}
VECTOR2F GetHitSegmentPosVec2(const VECTOR2F& A, const VECTOR2F& B, const VECTOR2F& C, const VECTOR2F& D)
{
	VECTOR2F vec_AB = B - A;
	float AB_dist = ToDistVec2(vec_AB);
	VECTOR2F vec_AC = C - A;
	VECTOR2F vec_CD = D - C;

	float crs_1 = CrossVec2(vec_AB, vec_CD);
	float crs_2 = CrossVec2(vec_AC, vec_CD);

	float t1 = crs_2 / crs_1;

	vec_AB *= t1;
	return A + vec_AB;
}

bool IsHitCircle(const VECTOR2F& pos1, const VECTOR2F& pos2, const float radius1, const float radius2)
{
	const float dist = DotVec2(pos1 - pos2, pos1 - pos2);
	if (dist < (radius1 + radius2)*(radius1 + radius2))
		return true;

	return false;
}

bool IsHitRect(const VECTOR2F& pos1, const VECTOR4F& size1, const VECTOR2F& pos2, const VECTOR4F& size2)
{
	if (pos1.x + size1.x < pos2.x + size2.z) return false;
	if (pos1.x + size1.z > pos2.x + size2.x) return false;
	if (pos1.y + size1.y < pos2.y + size2.w) return false;
	if (pos1.y + size1.w > pos2.y + size2.y) return false;

	return true;
}

bool IsHitCapsuleCircle(const VECTOR2F& start, const VECTOR2F& end, const VECTOR2F& c_pos, const float radius1, const float radius2)
{
	const float dist = GetCapsuleCircleDistance(start, end, c_pos);

	if (dist < (radius1 + radius2)*(radius1 + radius2))
		return true;

	return false;
}

float GetCapsuleCircleDistance(const VECTOR2F& startpos, const VECTOR2F& endpos, const VECTOR2F& pos)
{
	//�덷
	const float epsilon = 1.0e-5f;

	VECTOR2F segment_sub;
	segment_sub = endpos - startpos;

	VECTOR2F segment_point;
	segment_point = pos - startpos;

	if (DotVec2(segment_sub, segment_point) < epsilon)
	{
		return DotVec2(segment_point, segment_point);
	}

	segment_point = endpos - pos;
	if (DotVec2(segment_sub, segment_point) < epsilon)
	{
		return DotVec2(segment_point, segment_point);
	}

	const float cross_product = CrossVec2(segment_sub, segment_point);
	const float dist = cross_product * cross_product / DotVec2(segment_sub, segment_sub);
	return dist;
}
/*/2D����/*/


/*/3D����/*/
bool IsHitSphere(const VECTOR3F& pos1, const VECTOR3F& pos2, const float radius1, const float radius2)
{
	const float dist = DotVec3(pos1 - pos2, pos1 - pos2);
	if (dist < (radius1 + radius2)*(radius1 + radius2))
		return true;

	return false;
}

bool IsHitCube(const VECTOR3F& min1, const VECTOR3F& max1,
	const VECTOR3F& min2, const VECTOR3F& max2)
{
	if (min1.x > max2.x) return false;
	if (max1.x < min2.x) return false;
	if (min1.y > max2.y) return false;
	if (max1.y < min2.y) return false;
	if (min1.z > max2.z) return false;
	if (max1.z < min2.z) return false;
	
	return true;
}

bool IsHitCapsuleSphere(const VECTOR3F& start, const VECTOR3F& end, const VECTOR3F& s_pos, const float radius1, const float radius2)
{
	const float dist = GetCapsuleSphereDistance(start, end, s_pos);

	if (dist < (radius1 + radius2)*(radius1 + radius2))
		return true;

	return false;
}

float GetCapsuleSphereDistance(const VECTOR3F& start, const VECTOR3F& end, const VECTOR3F& s_pos)
{
	//�덷
	const float epsilon = 1.0e-5f;

	VECTOR3F segment_sub;
	segment_sub = end - start;

	VECTOR3F segment_point;
	segment_point = s_pos - start;

	if (DotVec3(segment_sub, segment_point) < epsilon)
	{
		return DotVec3(segment_point, segment_point);
	}

	segment_point = end - s_pos;
	if (DotVec3(segment_sub, segment_point) < epsilon)
	{
		return DotVec3(segment_point, segment_point);
	}

	const VECTOR3F cross_product = CrossVec3(segment_sub, segment_point);
	const float dist = DotVec3(cross_product, cross_product) / DotVec3(segment_sub, segment_sub);
	return dist;
}
/*/3D����/*/