#include "camera.h"
#include "input.h"
#include"enemy.h"
#ifdef USE_IMGUI
#include "./imgui/imgui.h"
#include "./imgui/imgui_impl_dx11.h"
#include "./imgui/imgui_impl_win32.h"
#include "./imgui/imgui_internal.h"
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

void Camera::Update(const VECTOR3F& pos, const VECTOR3F& rotation)
{
#ifdef USE_IMGUI
	ImGui::SetNextWindowSize(ImVec2(300, 200));
	ImGui::Begin("Camera");
	static const char*cameratype[] =
	{
		"Relative",
		"TPS",
		"Look-on",
	};
	if (ImGui::Combo("CameraType", &state, cameratype, (int)(sizeof(cameratype) / sizeof(*cameratype))))
	{
		if (state == 2)look_on_flag = true;
	}
	ImGui::Text("position.x%.2f\nposition.y%.2f\nposition.z%.2f", position.x, position.y, position.z);
	ImGui::Text("pos.x%.2f\npos.y%.2f\npos.z%.2f", pos.x, pos.y, pos.z);
	ImGui::Text("target.x%.2f\ntarget.y%.2f\ntarget.z%.2f", target.x, target.y, target.z);
	ImGui::End();
#endif
	switch (state)
	{
	case CAMERA_TYPE::Relative:
		Relative_move(pos);
		break;
	case CAMERA_TYPE::TPS:
		TPS_move(pos, rotation);
		break;
	case CAMERA_TYPE::Look_on:
		Look_on_move(pos);
		break;
	}
}

void Camera::Relative_move(const VECTOR3F&pos)
{
	static float angle = DirectX::XMConvertToRadians(180.f);
	static const float dangle = DirectX::XMConvertToRadians(1.0f);
	if (input::KeyboardManager::GetInstance().PressedState(input::KeyLabel::LEFT))
	{
		angle -= dangle;
	}
	if (input::KeyboardManager::GetInstance().PressedState(input::KeyLabel::RIGHT))
	{
		angle += dangle;
	}

	if (angle >= DirectX::XMConvertToRadians(360.f))
	{
		angle -= DirectX::XMConvertToRadians(360.f);
	}
	else if (angle <= DirectX::XMConvertToRadians(0.f))
	{
		angle += DirectX::XMConvertToRadians(360.f);
	}

	
	float dx = sinf(angle)*lenge.x,dz= cosf(angle)*lenge.z;
	static float p_z = pos.z;
	position = VECTOR3F(pos.x + dx, pos.y + lenge.y, pos.z + dz);
	//position.x = pos->x + dx;
	//position.y = pos->y + lenge.y;
	//position.z = pos->z + dz;
	target = VECTOR3F(pos.x, pos.y + 25.f, (pos.z - p_z));
}

void Camera::TPS_move(const VECTOR3F& pos, const VECTOR3F& rotation)
{
	position = VECTOR3F(pos.x + (sinf(rotation.y)*lenge.x), pos.y + lenge.y, pos.z + (cosf(rotation.y)*lenge.z));
	target = VECTOR3F(pos.x, pos.y + 25.f, pos.z);;
}

void Camera::Look_on_move(const VECTOR3F& pos)
{
	if (enm == nullptr)return;
	VECTOR3F v = enm->position - pos;
	v = NormalizeVec3(v);
	position.x = pos.x-(v.x*lenge.x);
	position.y = pos.y-(v.y*lenge.y)+45.f;
	position.z = pos.z-(v.z*lenge.z);
	target = enm->position;
	look_on_flag = false;
}

void Camera::SetLook_no(Enemy * enemy)
{
	
	if (state != CAMERA_TYPE::Look_on || !look_on_flag)return;
	
	static float leng;
	VECTOR3F v = enemy->position - position;
	float l = (v.x*v.x) + (v.y*v.y) + (v.z*v.z);

	if (enm == nullptr)
	{
		if (l <  90000.f)
		{
			enm = enemy;
			leng = l;
		}
	}
	else
	{
		if (leng < l)
		{
			enm = enemy;
			leng = l;
		}
	}

}

Camera::~Camera()
{
	if (enm != nullptr)
	{
		enm = nullptr;
	}
}
