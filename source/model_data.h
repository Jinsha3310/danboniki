#pragma once
#include <d3d11.h>
#include <wrl.h>
#include <string>
#include <vector>
#include <memory>

#include <fbxsdk.h>

#undef max
#undef min

#include <cereal/cereal.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/vector.hpp>

#include "vector.h"

template<class Archive>
void serialize(Archive& archive, VECTOR2F& vector)
{
	archive(vector.x, vector.y);
}

template<class Archive>
void serialize(Archive& archive, VECTOR3F& vector)
{
	archive(vector.x, vector.y, vector.z);
}

template<class Archive>
void serialize(Archive& archive, UVECTOR4& vector)
{
	archive(vector.x, vector.y, vector.z, vector.w);
}

template<class Archive>
void serialize(Archive& archive, VECTOR4F& vector)
{
	archive(vector.x, vector.y, vector.z, vector.w);
}

template<class Archive>
void serialize(Archive& archive, FLOAT4X4& float4x4)
{
	archive(
		float4x4._11, float4x4._12, float4x4._13, float4x4._14,
		float4x4._21, float4x4._22, float4x4._23, float4x4._24,
		float4x4._31, float4x4._32, float4x4._33, float4x4._34,
		float4x4._41, float4x4._42, float4x4._43, float4x4._44);
}

struct  ModelData
{
	struct Vertex
	{
		VECTOR3F position;
		VECTOR3F normal;
		VECTOR2F texcoord;
		VECTOR4F bone_weight;
		UVECTOR4 bone_index;

		template<class Archive>
		void serialize(Archive &archive)
		{
			archive(CEREAL_NVP(position),
				CEREAL_NVP(normal),
				CEREAL_NVP(texcoord),
				CEREAL_NVP(bone_weight),
				CEREAL_NVP(bone_index));
		}
	};
	struct Subset
	{
		int	material_index;
		int	index_start;
		int index_count;

		template<class Archive>
		void serialize(Archive &archive)
		{
			archive(CEREAL_NVP(material_index), CEREAL_NVP(index_start), CEREAL_NVP(index_count));
		}
	};
	struct Mesh
	{
		std::string name;

		std::vector<Vertex> vertices;
		std::vector<unsigned int> indices;
		std::vector<Subset> subsets;

		int bone_index;

		std::vector<int> bone_indices;
		std::vector<FLOAT4X4>	inverse_transforms;

		template<class Archive>
		void serialize(Archive &archive)
		{
			archive(CEREAL_NVP(name),
				CEREAL_NVP(vertices), CEREAL_NVP(indices), CEREAL_NVP(subsets),
				CEREAL_NVP(bone_index), CEREAL_NVP(bone_indices),
				CEREAL_NVP(inverse_transforms));
		}
	};

	std::vector<Mesh> meshes;
	
	struct Bone
	{
		std::string name;
		int			parent_index;
		VECTOR3F	scale;
		VECTOR4F	rotate;
		VECTOR3F	translate;

		template<class Archive>
		void serialize(Archive &archive)
		{
			archive(CEREAL_NVP(name),
				CEREAL_NVP(parent_index),
				CEREAL_NVP(scale),
				CEREAL_NVP(rotate),
				CEREAL_NVP(translate));
		}
	};
	std::vector<Bone> bones;

	struct Material
	{
		std::wstring name;

		VECTOR4F color;
		template<class Archive>
		void serialize(Archive &archive)
		{
			archive(CEREAL_NVP(name),
				CEREAL_NVP(color));
		}
	};
	std::vector<Material> materials;

	struct BoneData
	{
		VECTOR3F	scale;
		VECTOR4F	rotate;
		VECTOR3F	translate;

		template<class Archive>
		void serialize(Archive &archive)
		{
			archive(CEREAL_NVP(scale),
				CEREAL_NVP(rotate),
				CEREAL_NVP(translate));
		}
	};
	struct KeyFrame
	{
		std::vector<BoneData> bone_datas;
		template<class Archive>
		void serialize(Archive &archive)
		{
			archive(CEREAL_NVP(bone_datas));
		}
	};
	struct Animation
	{
		std::vector<KeyFrame> keyframes;
		template<class Archive>
		void serialize(Archive &archive)
		{
			archive(CEREAL_NVP(keyframes));
		}
	};
	std::vector<Animation> animations;

	struct AnimationTake
	{
		std::string name;
		int sampling_rate;
		int number_of_frames = 0;

		template<class Archive>
		void serialize(Archive &archive)
		{
			archive(CEREAL_NVP(name), CEREAL_NVP(sampling_rate), CEREAL_NVP(number_of_frames));
		}
	};
	std::vector<AnimationTake> animation_takes;

	template<class Archive>
	void serialize(Archive &archive)
	{
		archive(CEREAL_NVP(meshes),
			CEREAL_NVP(bones), CEREAL_NVP(materials),
			CEREAL_NVP(animations), CEREAL_NVP(animation_takes));
	}
};

class ModelResource
{
public:
	ModelResource(ID3D11Device* device, std::unique_ptr<ModelData> data);
	~ModelResource() {};

	struct Material
	{
		VECTOR4F color = { 0.8f, 0.8f, 0.8f, 1.0f };
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> shader_resource_view;
		bool CreateShaderResourceView(ID3D11Device* device, std::wstring& texname);
	};

	struct Subset
	{
		unsigned int index_start = 0; 
		unsigned int index_count = 0;
		Material* material;
	};

	struct Mesh
	{
		Microsoft::WRL::ComPtr<ID3D11Buffer> vertex_buffer;
		Microsoft::WRL::ComPtr<ID3D11Buffer> index_buffer;
		std::vector<Subset> subsets;

		int bone_index;
		std::vector<int> bone_indices;
		std::vector<FLOAT4X4>			inverse_transforms;

		bool CreateBuffers(ID3D11Device* device, ModelData::Vertex* vertices, unsigned int vertex_num, unsigned int* indeces, unsigned int index_num);
	};

	const std::vector<ModelData::Bone>& GetBones()  const { return data->bones;  }
	const std::vector<Mesh>& GetMeshes() const { return meshes; }
	const std::vector<ModelData::Animation>& GetAnimations() const { return data->animations; }
	const std::vector<ModelData::AnimationTake>& GetAnimationTakes() const { return data->animation_takes; }

private:
	std::unique_ptr<ModelData> data;
	std::vector<Material> materials;
	std::vector<Mesh> meshes;
};

void GetResourcePath(char(&combinedResourcePath)[256], const char* referrer_filename, const char* referent_filename);
void GetResourcePathW(wchar_t(&combinedResourcePath)[256], const wchar_t* referrer_filename, const wchar_t* referent_filename);