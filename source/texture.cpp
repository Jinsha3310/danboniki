#include "texture.h"
#include "framework.h"

namespace texture
{
	Texture::Texture() : sprBatch(nullptr)
	{
	}

	Texture::~Texture()
	{
		Release();
	}

	bool Texture::LoadTexture(
		const wchar_t* filename, unsigned int maxinstance)
	{
		sprBatch = new SpriteBatch(FrameWork::GetDevice(), filename, maxinstance);
		return (sprBatch != nullptr);
	}

	void Texture::Release()
	{
		if (sprBatch != nullptr)
		{
			delete sprBatch;
			sprBatch = nullptr;
		}
	}

	void Texture::Begin(ID3D11DeviceContext* context)
	{
		sprBatch->Begin(context);
	}

	void Texture::End(ID3D11DeviceContext* context)
	{
		sprBatch->End(context);
	}

	void Texture::Render(const VECTOR2F& pos, const VECTOR2F& scale,
		const VECTOR2F& tex_pos, const VECTOR2F& tex_size,
		const VECTOR2F& center,
		const float angle,
		const VECTOR4F& colour, bool reverse)
	{
		if (sprBatch)
			sprBatch->Render(pos, scale, tex_pos, tex_size, center, angle, colour, reverse);
	}

	bool Texture::isEmpty()
	{
		return (sprBatch == nullptr);
	}

	int TextureManager::LoadTexture(const wchar_t* filename, unsigned int maxinstance)
	{
		++handle_num;

		texture_data[handle_num] = std::make_unique<Texture>();
		if (texture_data[handle_num]->isEmpty())
		{
			if (texture_data[handle_num]->LoadTexture(filename, maxinstance))
			{
				return handle_num;
			}
		}

		return -1;
	}

	void TextureManager::Begin(int num)
	{
		texture_data[num]->Begin(FrameWork::GetContext());
	}

	void TextureManager::End(int num)
	{
		texture_data[num]->End(FrameWork::GetContext());
	}

	void TextureManager::Render(int num, const VECTOR2F& position, const VECTOR2F& scale,
		const VECTOR2F& texPos, const VECTOR2F& texSize,
		const VECTOR2F& center,
		const float angle,
		const VECTOR4F& colour,
		bool reverse)
	{
		texture_data[num]->Render(position, scale, texPos, texSize, center, angle, colour, reverse);
	}

}