#include <fstream>
#include <./DirectXTK/DirectXTK-master/Inc/WICTextureLoader.h>
#include <assert.h>

#include "resource.h"

std::map<std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>	ResourceManager::textures;
std::map<std::string,  ResourceManager::VertexShader_and_InputLayout>		ResourceManager::vertex_shaders;
std::map<std::string,  Microsoft::WRL::ComPtr<ID3D11PixelShader>> 			ResourceManager::pixel_shaders;

bool LoadFile(const char* filename, char** blob, unsigned int& size)
{
	using namespace std;

	ifstream inputfile(filename, ifstream::binary);
	inputfile.seekg(0, ifstream::end);
	size = static_cast<int>(inputfile.tellg());
	inputfile.seekg(0, ifstream::beg);
	*blob = new char[size];
	inputfile.read(*blob, size);
	inputfile.close();

	if (!inputfile)
		return false;
	return true;
}

bool ResourceManager::LoadShaderResourceViews(ID3D11Device* device, const wchar_t* filename,
	ID3D11ShaderResourceView** srv, D3D11_TEXTURE2D_DESC* texdesc)
{
	ID3D11Resource* resource = nullptr;

	std::wstring wstr(filename);

	//データ検索
	std::map<std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>::iterator it = textures.find(wstr);
	if (it != textures.end())
	{
		//見つけた！！！
		*srv = it->second.Get();
		(*srv)->AddRef();
		it->second.Get()->GetResource(&resource);
	}	
	else
	{
		HRESULT hr = S_OK;
		hr = DirectX::CreateWICTextureFromFile(device, filename, &resource, srv);
		if (FAILED(hr))
		{
			assert(!"CreateWICTextureFromFile error");
			return false;
		}

		textures.insert(std::make_pair(wstr, *srv));//新データ作る
	}

	//texture_2Dの取得　検索？
	ID3D11Texture2D *texture_2D;
	if (FAILED(resource->QueryInterface(&texture_2D)))
	{
		assert(!"QueryInterface error");
		resource->Release();
		return false;
	}
	texture_2D->GetDesc(texdesc);
	texture_2D->Release();
	resource->Release();

	return true;
}

void ResourceManager::ReleaseShaderResourceViews(std::wstring wstr)
{
	std::map<std::wstring, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>>::iterator it = textures.find(wstr);
	if (it != textures.end())
	{
		textures.erase(it);
	}
	else {
		assert(!"シェーダリソースビューのファイルネームが見つかりませんでした。");
	}
}

//頂点シェーダの読み込み
bool ResourceManager::LoadVertexShaders(ID3D11Device* device, const char* filename,
	D3D11_INPUT_ELEMENT_DESC *elementd_desc, int num_element,
	ID3D11VertexShader** vs, ID3D11InputLayout** il)
{
	*vs = nullptr;
	*il = nullptr;

	
	//csoファイルのロード
	std::map<std::string, ResourceManager::VertexShader_and_InputLayout>::iterator it = vertex_shaders.find(filename);
	if (it != vertex_shaders.end())//データ検索
	{
		//見つけた！！！
		*vs = it->second.vertex_shader.Get();
		(*vs)->AddRef();

		*il = it->second.input_layout.Get();
		(*il)->AddRef();
		
		
		return true;
	}
		

	char *blob;
	unsigned int size = 0;

	// コンパイル済みピクセルシェーダーオブジェクトの読み込み
	if (!LoadFile(filename, &blob, size))
		return false;

	HRESULT hr = S_OK;
	// 頂点シェーダーオブジェクトの生成
	hr = device->CreateVertexShader(blob, size, nullptr, vs);
	if (FAILED(hr))
	{
		OutputDebugStringA("********************************");
		OutputDebugStringA("CreateVertexShader でエラー");
		OutputDebugStringA("********************************");
		assert(!"CreateVertexShader error");
		return false;
	}

	// 入力レイアウトの作成
	hr = device->CreateInputLayout(elementd_desc, num_element, blob, size, il);
	if (FAILED(hr))
	{
		OutputDebugStringA("********************************");
		OutputDebugStringA("CreateInputLayout でエラー");
		OutputDebugStringA("********************************");
		assert(!"CreateInputLayout  error");
		return false;
	}
	delete[] blob;

	vertex_shaders.insert(std::make_pair(filename, VertexShader_and_InputLayout(*vs, *il)));

	return true;
}

void ResourceManager::ReleaseVertexShaders(std::string str)
{

	std::map<std::string, ResourceManager::VertexShader_and_InputLayout>::iterator it = vertex_shaders.find(str);
	if (it != vertex_shaders.end())
	{		
		vertex_shaders.erase(it);
	}
	else {
		assert(!"頂点シェーダのファイルネームが見つかりませんでした。");
	}
}


bool ResourceManager::LoadPixelShaders(ID3D11Device* device, const char* filename, ID3D11PixelShader** ps)
{
	*ps = nullptr;


	//csoファイルのロード
	std::map<std::string, Microsoft::WRL::ComPtr<ID3D11PixelShader>>::iterator it = pixel_shaders.find(filename);
	if (it != pixel_shaders.end())
	{
		*ps = it->second.Get();
		(*ps)->AddRef();
		return true;
	}
		
	char *blob;
	unsigned int size = 0;

	// コンパイル済みピクセルシェーダーオブジェクトの読み込み
	if (!LoadFile(filename, &blob, size))
		return false;

	HRESULT hr = device->CreatePixelShader(blob, size, nullptr, ps);
	if (FAILED(hr)) {
		OutputDebugStringA("********************************");
		OutputDebugStringA("CreatePixelShader でエラー");
		OutputDebugStringA("********************************");


		assert(!"CreatePixelShader error");
		return false;
	}

	delete[] blob;

	pixel_shaders.insert(std::make_pair(filename, *ps));

	return true;
}


void ResourceManager::ReleasePixelShaders(std::string str)
{

	std::map<std::string, Microsoft::WRL::ComPtr<ID3D11PixelShader>>::iterator it = pixel_shaders.find(str);
	if (it != pixel_shaders.end())
	{
		pixel_shaders.erase(it);
	}
	else {
		assert("ピクセルシェーダーのファイルネームが見つかりませんでした。");
	}
}