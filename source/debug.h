#pragma once
#include <memory>


#include "sprite.h"
#include "resource.h"

class Debug : public SpriteBatch{
private:
	//std::queue<std::string> debugQueue;
	//std::unique_ptr<SpriteBatch> debug_font;
	
	float posY;
	int screenHeight;
public:
	Debug(ID3D11Device*, const wchar_t*, unsigned int maxInstance = 8192U, int screen_h = 480);
	~Debug();

	//void SetString(const char*, ...);	//デバッグ文字列の設定

	//デバッグ文字列の表示
	void DrawString(ID3D11DeviceContext* context, float posX /*= 0.f*/, float posY /*= 0.f*/, float r /*= 1.f*/, float g /*= 1.f*/, float b /*= 1.f*/, float scaleX /*= 1.f*/, float scaleY /*= 1.f*/, const char* str, ...);
};
