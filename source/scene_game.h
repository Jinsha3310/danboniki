#pragma once

#include "scene.h"


#include <memory>
#include <thread>
#include <mutex>

class SceneGame : public Scene
{
	std::unique_ptr<std::thread> loading_thread;
	std::mutex loading_mutex;

	

	DirectX::XMFLOAT4X4 view_projection;
	VECTOR4F light;
public:
	~SceneGame()
	{
		EndLoading();
	}

	bool IsNowLoading()
	{
		if (loading_thread && loading_mutex.try_lock())
		{
			loading_mutex.unlock();
			return false;
		}
		return true;
	}

	void EndLoading()
	{
		if (loading_thread && loading_thread->joinable())
		{
			loading_thread->join();
		}
	}

	bool Initialize(ID3D11Device *device);
	const char* Update(float elapsed_time);
	void Render(ID3D11DeviceContext* context);
	void Finalize();
};